# AZ-US-xxappxx
The az-us-xxappxx-template bit bucket repository is a template which would be used for creating a
bit bucket repository for a given application.

Please goto https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
for detailed practises for maintaining a bit bucket repository

Files and Configurations:
-------------------------
tox.ini: will have the dependencies and automation configurations required for the tests to run.

tox is a generic virtualenv management and test command line tool you can use for:

checking your package installs correctly with different Python versions and interpreters
running your tests in each of the environments, configuring your test tool of choice
acting as a frontend to Continuous Integration servers, greatly reducing boilerplate
and merging CI and shell-based testing.

Sharp.ini : contains details on airflow dag

setup.py : will have all the required configurations to build a given project

Folders and Packages:
---------------------
tests : Contains bunch of pytests aiding in asserting the correctness of the config and py files
under xxappxx/config and xxappxx/etl respectively

xxappxx : contains project specific config, ddl, etl code
    xxappxx/aws : contains all the config related to AWS policies
    xxappxx/config : Contains all the project specific configuration files in yaml format.
        xxappxx/config/main.yaml : Contains config related to source file name pattern, date and time pattern
                                   of the source files and airflow schedule details.
        xxappxx/config/emr.yaml : contains config related to EMR settings (storage, memory, cluster size and node type)
        xxappxx/config/L0DataSets.yaml : contains config related to l0 table locations and file mapping,
                                         table repair, drop and create table config.
        xxappxx/config/L1DataSets.yaml : contains config related to l1 tables
        xxappxx/config/L1DataSets.yaml : contains config related to l2 tables
        xxappxx/config/DPDataSets.yaml : contains config related to data package tables
    xxappxx/ddl :
        xxappxx/ddl/dp : conatins ddls for data package layer
        xxappxx/ddl/l0 : conatins ddls for l0 layer
        xxappxx/ddl/l1 : conatins ddls for l1 layer
        xxappxx/ddl/l2 : conatins ddls for l2 layer
    xxappxx/etl : contains all the ETL code used for loading l1,l2 and dp
        xxappxx/ddl/dp : conatins etl for data package layer
        xxappxx/ddl/l0 : conatins etl for l0 layer
        xxappxx/ddl/l1 : conatins etl for l1 layer
        xxappxx/ddl/l2 : conatins etl for l2 layer


Note : All the place holders are mentioned in comments in individual files
       that have to replaced with project specifics.
