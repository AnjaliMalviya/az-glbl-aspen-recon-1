import ast
import os

import pytest
import recon_dshbrd
from pathlib import Path

def is_valid_python_file(fname):
    with open(fname) as f:
        contents = f.read()
    try:
        ast.parse(contents)
        compile(contents, fname, 'exec', ast.PyCF_ONLY_AST)
        return True
    except SyntaxError as e:
        raise e


def file_list():
    return list(Path(os.path.dirname(recon_dshbrd.__file__)).rglob("*.py"))

@pytest.mark.parametrize("file", file_list())
def test_valid_python_file(file):
    is_valid_python_file(file)
