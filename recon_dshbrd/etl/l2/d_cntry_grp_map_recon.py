def run(*args,spark,static_file_location,curr_cycle_id,s3_path,folder_level_1,folder_level_2,folder_level_3,
        to_table_name,to_schema_name,s3_path_publish, folder_level_1_publish, folder_level_2_publish,
        folder_level_3_publish, schema_name_publish,**kwargs):

    # Read the CSV file for country mapping
    df_cntry_mapping=spark.read.option("header", "true").option("delimiter","|").csv("{static_file_location}".format(static_file_location=static_file_location))
    df_cntry_mapping.registerTempTable("df_cntry_mapping")

    # fetch the data date from cycle id
    actual_data_dt = str(curr_cycle_id)
    actual_data_dt = actual_data_dt[0:8]

    df_cntry_map=spark.sql("""
    select ctry_cd,ctry_grp,ctry_nm,
    "" as flex_1,"" as flex_2,"" as flex_3,"" as flex_4,"" as flex_5,{data_dt} as data_dt,	
    {curr_cycle_id} as cycl_id
    from df_cntry_mapping
    """.format(curr_cycle_id=curr_cycle_id, data_dt=actual_data_dt))

    df_cntry_map.registerTempTable("df_cntry_map")

    #push the data on datalake and publish
    df_cntry_map.write.mode("overwrite").partitionBy("data_dt", "cycl_id").parquet(
        "{s3_path}/{folder_level_1}/{folder_level_2}/{folder_level_3}/{to_table_name}/".format(
            to_table_name=to_table_name, s3_path=s3_path, folder_level_1=folder_level_1,
            folder_level_2=folder_level_2, folder_level_3=folder_level_3))

    spark.sql("""ALTER TABLE {to_schema_name}.{to_table_name} recover partitions""".format(
        to_schema_name=to_schema_name, to_table_name=to_table_name))

    spark.sql("""ALTER TABLE {to_schema_name}.{to_table_name} recover partitions""".format(
        to_schema_name=to_schema_name, to_table_name=to_table_name))

    partitions = spark.sql("SHOW PARTITIONS {schema_name_publish}.{to_table_name} ".format(schema_name_publish=schema_name_publish,
                                                                                      to_table_name=to_table_name))
    listpartitions = list(partitions.select('partition').toPandas()['partition'])
    print(listpartitions)
    listpartition = []
    for a in listpartitions:
        a = a.replace('/cycl_id', '')
        listpartition.append(a)
    print(listpartition)
    cleanpartitions = [i.split('=')[1] for i in listpartition]
    actual_data_dt = actual_data_dt
    filtered = [i for i in cleanpartitions if i <= str(actual_data_dt)]
    print(filtered)

    for i in filtered:
        spark.sql("""alter table {schema_name_publish}.{to_table_name} DROP IF EXISTS PARTITION (data_dt = {i})""".format(
            schema_name_publish=schema_name_publish, to_table_name=to_table_name, i=i))

    df_cntry_map.write.mode("overwrite").partitionBy("data_dt", "cycl_id").parquet(
        "{s3_path_publish}/{folder_level_1_publish}/{folder_level_2_publish}/{folder_level_3_publish}/{to_table_name}/".format(
            to_table_name=to_table_name, s3_path_publish=s3_path_publish, folder_level_1_publish=folder_level_1_publish,
            folder_level_2_publish=folder_level_2_publish, folder_level_3_publish=folder_level_3_publish))

    spark.sql("""ALTER TABLE {schema_name_publish}.{to_table_name} recover partitions""".format(
        schema_name_publish=schema_name_publish, to_table_name=to_table_name))
