def run(*args, spark, static_file_location, schema_f_spend, f_spend, schema_name_cnrty_map, table_name_cntry_map,
        schema_fact_dq, fact_dq, s3_path, datalake_bucket, prefix_path, schema_d_cmmn_txnmy, d_cmmn_txnmy,
        prefix_path_dq_recon, to_table_name, s3_path_publish,folder_level_1,folder_level_2,folder_level_3,
        folder_level_1_publish, folder_level_2_publish,
        folder_level_3_publish,schema_name_publish,curr_cycle_id, datalake_bucket_cust,to_schema_name,**kwargs):

    import os
    from pyspark.sql.types import StructType, StructField, StringType
    import pyspark.sql.functions as F
    from pyspark.sql.functions import lit
    import functools
    from pyspark.sql import DataFrame
    import pandas as pd
    import datetime
    from datetime import date, timedelta
    from pyspark.sql.functions import dayofweek
    from boto3 import client
    conn = client('s3')

    cycl_id_list = []
    result = conn.list_objects(Bucket='{datalake_bucket}'.format(datalake_bucket=datalake_bucket),
                               Prefix='{prefix_path}'.format(prefix_path=prefix_path), Delimiter='/')
    for o in result.get('CommonPrefixes'):
        data_path = o.get('Prefix').split('/')
        cycl_id_list.append(data_path[3])
    cycl_id_set = set(cycl_id_list)
    cycl_id_list = list(cycl_id_set)
    for i in range(0, len(cycl_id_list)):
        cycl_id_list[i] = cycl_id_list[i].replace("cycl_id=", "")
        cycl_id_list[i] = int(cycl_id_list[i])
    cycl_id_list.sort(reverse=True)
    max_cycl_id = str(cycl_id_list[1])
    max_cycl_yyyymmdd = max_cycl_id[0:4] + "-" + max_cycl_id[4:6] + "-" + max_cycl_id[6:8]

    list7 = []
    for i in cycl_id_list:
        cycl_1 = str(i)
        cycl_1_yyyymmdd = cycl_1[0:4] + "-" + cycl_1[4:6] + "-" + cycl_1[6:8]
        if (cycl_1_yyyymmdd == max_cycl_yyyymmdd):
            list7.append(i)
            date1 = datetime.datetime.strptime(max_cycl_yyyymmdd, "%Y-%m-%d").date()
            date1 = date1 - timedelta(7)
            max_cycl_yyyymmdd = str(date1)
        else:
            pass
    print(list7)
    # Read config file from s3 which contains details of cols to be compared
    df_cols = spark.read.option("header", "true").option("delimiter", ",").csv("{static_file_location}".format(
        static_file_location=static_file_location))
    df_cols.registerTempTable("df_cols")

    # ALL RECORDS FROM SPEND_FILTERED_UPDTS
    dq_recon_filtered = spark.sql("""
    select * from gbl_aspen_datalake_app_commons_test.f_spend_flt_updts where recipient_country_cd not like 'UNK_CNTRY'
     """)
    dq_recon_filtered.registerTempTable("dq_recon_filtered")

    # Correction recording from f_spend
    df_recon_filtered_corr = spark.sql("""
    select * from dq_recon_filtered where winning_expense_id <> org_expense_id
    """)

    df_recon_filtered_corr.registerTempTable("df_recon_filtered_corr")

    # Dataframe for correction coming from AU, US for VEEVA,VWP and ESTR
    df_recon_filtered_src_updt = spark.sql("""
    select * from dq_recon_filtered where winning_expense_id = org_expense_id and source_system_nm  in ('VWP','ESTR') 
    or (source_system_nm  in ('VEEVA') and recipient_country in ('AU','US'))""")
    df_recon_filtered_src_updt.registerTempTable("df_recon_filtered_src_updt")

    df_corr_src_upt_union = spark.sql("""
    select * from df_recon_filtered_corr
    union
    select * from df_recon_filtered_src_updt
    """)

    df_corr_src_upt_union.registerTempTable("df_corr_src_upt_union")

    # DF with no correction records
    df_updt_no_corr = spark.sql("""
    select * from dq_recon_filtered
    except
    select  * from df_corr_src_upt_union
    """)

    df_updt_no_corr.registerTempTable("df_updt_no_corr")

    # no correction final dataframe

    df_spend_final_no_corr = spark.sql("""
        select 
        a.spnd_id,
        a.expense_id,
        a.org_expense_id,
        a.winning_expense_id,
        a.az_cust_id,
        a.source_system_nm,
        a.recipient_country_cd,
        a.record_company_cd,
        a.cust_full_nm,
        a.spend_date,
        a.submit_dt,
        a.spend_txnmy_id,
        a.row_updt_dt as updt_time,
        a.author_full_nm as updt_by,
        a.spend_status_desc as updt_reason,
        "No Updates" as comp_col,
        "" as updt_val,
        "" as prev_val,
        "Y"  as latest_exp_flg
        from df_updt_no_corr a
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19
        """)
    df_spend_final_no_corr.registerTempTable("df_spend_final_no_corr")

    df_cols = spark.read.option("header", "true").option("delimiter", ",").csv("{static_file_location}".format(
        static_file_location=static_file_location))
    df_cols.registerTempTable("df_cols")

    df_col = spark.sql("""select ctrl_column from df_cols where lower(ctl_type)='comp_col' group by 1""")
    df_col.registerTempTable("df_col")

    col_list = df_col.select('ctrl_column').collect()

    col_lists = []
    for c in col_list:
        c = str(c).replace('Row(ctrl_column=\'', '')
        c = str(c).replace('\''')', '')
        col_lists.append(c)

    lst_col_fetch = ['spnd_id', 'expense_id', 'org_expense_id', 'row_updt_dt', 'author_full_nm', 'spend_status_desc',
                     'az_cust_id', 'source_system_nm', 'recipient_country_cd', 'record_company_cd', 'spend_date',
                     'submit_dt', 'spend_txnmy_id']

    lst_col_fetch.extend(col_lists)

    df_col = spark.sql("""select ctrl_val from df_cols where lower(ctrl_param)='cycl_id_cnt' group by 1""")
    df_col.registerTempTable("df_col")

    col_list = df_col.select('ctrl_val').collect()

    cnt_lists = []
    for c in col_list:
        c = str(c).replace('Row(ctrl_val=\'', '')
        c = str(c).replace('\''')', '')
        cnt_lists.append(c)
    output_dfs = []

    cnt = int(cnt_lists[0])

    mySchema = StructType([StructField(c, StringType()) for c in lst_col_fetch])
    df_spend_all = spark.createDataFrame(data=[], schema=mySchema)
    df_spend_all.registerTempTable("df_spend_all")

    for i in range(0, cnt):
        prefix_path_cycl_id = "dp/spend/f_spend/" + "cycl_id=" + str(cycl_id_list[i]) + "/"
        prefix_path_final = "s3://" + "az-gbl-aspen-datalake-test" + "/" + prefix_path_cycl_id
        print(prefix_path_final)
        df_spend_datas = spark.read.parquet(prefix_path_final)
        df_spend_datas = df_spend_datas.select(*lst_col_fetch)
        df_spend_datas.registerTempTable("df_spend_datas")

        output_dfs.append(df_spend_datas)

    # update condition as present in line no 79
    df_spend_all = functools.reduce(DataFrame.union, output_dfs)
    li = ['VWP', 'ESTR']
    ct = ['AU', 'US']
    df_spend_all = df_spend_all.filter(df_spend_all.source_system_nm.isin(li) | (
                df_spend_all.source_system_nm.isin('VEEVA') & df_spend_all.recipient_country_cd.isin(ct)))
    df_spend_all.registerTempTable("df_spend_all")

    df_spend_src_updt_all = spark.sql("""
    select b.* from 
    df_spend_all a 
    inner join 
    df_recon_filtered_src_updt b
    on a.spnd_id = b.spnd_id
    """)
    df_spend_src_updt_all.registerTempTable("df_spend_src_updt_all")

    df_spend_src_updt_all_rnk = spark.sql("""
    select spnd_id,expense_id,parent_expense_id,az_cust_id,national_id,src_cust_id,cust_src_sys,source_system_nm,
    rec_sys_cd,record_company_cd,spend_txnmy_id,az_prod_id,src_prod_id,ndc11_prod_id,brand_cd,spend_channel_cd,
    spend_channel_nm,spend_status_cd,spend_status_desc,currency_cd,spend_date,author_id,author_email,author_channel,
    author_first_nm,author_last_nm,author_middle_nm,author_full_nm,author_az_empl_id,fulfilment_vendor_id,
    fulfilment_vendor_nm,fulfilment_vendor_prid,fulfilment_vendor_addr_line_1,fulfilment_vendor_addr_line_2,
    fulfilment_vendor_addr_line_3,fulfilment_vendor_city,fulfilment_vendor_state,fulfilment_vendor_zip,
    fulfilment_vendor_zip_extn,fulfilment_vendor_country,submit_dt,start_dt,end_dt,payment_dt,field_activity_cd,
    event_desc,short_notes,long_notes,payment_method_supplement,atendee_role,speaker_nm,exhibit_nm,expense_payee_nm,
    expense_report_num,po_num,expense_avg_per_guest,advt_nm,advt_tgt_audience,advt_expense_cd_type,advt_expense_desc,
    advt_no_of_targets,advt_cost_per_piece,advt_circ_percentage,advt_total_cost,promo_item_qty,fmv,spend_amount,
    spd_amt_pre_alloc,spd_amt_total_participants,spd_amt_total_lic_hcps,spd_amt_total_prescribers,total_hcp,
    total_participants,total_azemployees,total_hcpstaff,total_speakers,total_no_of_noshows,total_licenced_hcp,
    row_load_dt,row_updt_dt,src_sys_activity_num,total_prescriber,shipped_qty,requested_qty,std_grp_num,std_legal_cost,
    item_id,unit_of_measure,activity_type_cd,activity_type_nm,act_category_nm,act_subcategory_nm,region_cd,
    item_src_sys_nm,program_id,project_number,supplier_invoice_number,allocation_number,site_id,pi_id,entity_paid_id,
    study_code,study_name,cor_ind,org_expense_id,persn_typ_id,orderid,cncr_prep_id,source_system_program_id,
    azer_short_notes,field_activity_code,cncr_src_sys_prgm_id,prep_id,createdondate_orderdate,cncr_expense_report_number,
    der_supp_invce_nbr,der_wbs_code,der_src_sys_prog_id_num,der_shipped_date,activity_code,ade_file_name,comments,
    contract_nbr,controlling_area,desc_of_meeting_event,doc_type_code,document_pay_amount,engagement_owner,
    eur_spend_amount,fiscal_year,internal_order,orgnl_txnmy_id,posting_key,recipient_country,rptbl_spend_amount,
    rptbl_spend_currency_cd,rptblty_ind,sap_doc_nbr,source_currency,usd_spend_amount,vendor_number,vendor_type,
    spd_chn_cd,sort2,event_id,payment_to,service_type,event_type,err_msg,sap_doc_no,nature_of_payment,joint_wrk_url,
    joint_working_url,transaction_id,text1,text2,text3,sap_vendor_name1,sap_vendor_name2,sap_vendor_country_code,
    biological_ind,last_modified_dt,exclusion_ind,nl_activity_type,contract_dt,department,jp_study_code,other_participants,
    case_report,number_of_cases,competitive_product,trail_name,approval_number,approval_date,sub_to_reexamination,
    kr_expense_category,kr_expense_type,venue_name,doc_dt,vendor_line_text,po_type,po_reference,meco_id,hospital_code,
    sap_vendor_region,location_name,total_grants,sap_vendor_name3,sap_vendor_name4,therapeutic_area,assignment,
    sample_code,ven_corp_grp,sap_tax_number1,history_fncn,text4,ods_ins_date,tax_code,tax_amount,attendee_type,
    delta_insert_flag,dcf_code,institution,position,name_kanji,name_kana,flag,contract_id,recipient_country_cd,
    cust_full_nm,cust_full_addr,phone_num,cust_class,email_id,cust_type_id,cust_sub_type_id,attendee_company,
    venue_address,cost_center_cd,sln_states,reportability_cd,consent_year,consent_flag,winning_expense_id, 
    dense_rank() over (partition by spnd_id order by row_updt_dt desc) as row_n
    from df_spend_src_updt_all a
    """)

    df_spend_src_updt_all_rnk.registerTempTable("df_spend_src_updt_all_rnk")

    output_dfs = []

    for col in col_lists:
        lst_col_fetch = ['spnd_id', 'expense_id', 'org_expense_id', 'winning_expense_id', 'row_n', 'row_updt_dt',
                         'author_full_nm', 'spend_status_desc', 'az_cust_id', 'source_system_nm',
                         'recipient_country_cd',
                         'record_company_cd', 'spend_date', 'submit_dt', 'cust_full_nm', 'spend_txnmy_id']

        lst_col_fetch.append(col)

        df_spend_data = df_recon_filtered_corr.select(*lst_col_fetch)
        df_spend_data = df_spend_data.withColumn("comp_col", lit(col))
        df_spend_data.registerTempTable("df_spend_data")

        # fetch the prev and updated value
        df_f_spend_val_prev_curr = spark.sql("""
        select * from (
        select 
        a.spnd_id,
        a.expense_id,
        a.org_expense_id,
        a.winning_expense_id,
        a.az_cust_id,
        a.source_system_nm,
        a.recipient_country_cd,
        a.record_company_cd,
        a.cust_full_nm,
        a.spend_date,
        a.submit_dt,
        a.spend_txnmy_id,
        a.row_updt_dt as updt_time,
        a.author_full_nm as updt_by,
        a.spend_status_desc as updt_reason,
        a.comp_col,
        a.{col} as updt_val,
        coalesce(b.{col},'') as prev_val,
        case when a.row_n='1' then 'Y' else 'N' end as latest_exp_flg
        from df_spend_data a
        left join
        df_spend_data b
        on
        a.org_expense_id=b.org_expense_id
        and
        a.winning_expense_id=b.winning_expense_id
        and a.row_n+1=b.row_n
        )
        """.format(col=col))

        df_f_spend_val_prev_curr.registerTempTable("df_f_spend_val_prev_curr")

        output_dfs.append(df_f_spend_val_prev_curr)

        df_spend_data = df_spend_src_updt_all_rnk.select(*lst_col_fetch)
        df_spend_data = df_spend_data.withColumn("comp_col", lit(col))
        df_spend_data.registerTempTable("df_spend_data")

        # fetch the prev and updated value for source data
        df_f_spend_val_prev_curr = spark.sql("""
        select * from (
        select 
        a.spnd_id,
        a.expense_id,
        a.expense_id as org_expense_id,
        a.expense_id as winning_expense_id,
        a.az_cust_id,
        a.source_system_nm,
        a.recipient_country_cd,
        a.record_company_cd,
        a.cust_full_nm,
        a.spend_date,
        a.submit_dt,
        a.spend_txnmy_id,
        a.row_updt_dt as updt_time,
        a.author_full_nm as updt_by,
        a.spend_status_desc as updt_reason,
        a.comp_col,
        a.{col} as updt_val,
        coalesce(b.{col},'') as prev_val,
        case when a.row_n='1' then 'Y' else 'N' end as latest_exp_flg
        from df_spend_data a
        left join
        df_spend_data b
        on
        a.spnd_id = b.spnd_id
        and a.row_n+1=b.row_n
        )
        """.format(col=col))

        df_f_spend_val_prev_curr.registerTempTable("df_f_spend_val_prev_curr")

        output_dfs.append(df_f_spend_val_prev_curr)

    df_spend_val = functools.reduce(DataFrame.union, output_dfs)
    df_spend_val.registerTempTable("df_spend_val")

    df_spend_val_update = spark.sql("""
    select * from df_spend_val where updt_val <> prev_val
    """)
    df_spend_val_update.registerTempTable("df_spend_val_update")

    df_spend_val_source_updts = spark.sql("""
    select 
    spnd_id,
    expense_id,
    org_expense_id,
    winning_expense_id,
    az_cust_id,
    source_system_nm,
    recipient_country_cd,
    record_company_cd,
    cust_full_nm,
    spend_date,
    submit_dt,
    spend_txnmy_id,
    updt_time,
    updt_by,
    updt_reason,
    latest_exp_flg
    from df_spend_val
    group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16
    """)
    df_spend_val_source_updts.registerTempTable("df_spend_val_source_updts")  # 9659

    df_spend_val_update_int = spark.sql(""" 
    select 
    spnd_id,
    expense_id,
    org_expense_id,
    winning_expense_id,
    az_cust_id,
    source_system_nm,
    recipient_country_cd,
    record_company_cd,
    cust_full_nm,
    spend_date,
    submit_dt,
    spend_txnmy_id,
    updt_time,
    updt_by,
    updt_reason,
    latest_exp_flg
    from df_spend_val_update
    group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16""")
    df_spend_val_update_int.registerTempTable("df_spend_val_update_int")

    df_spend_val_intmd = spark.sql("""
    select * from df_spend_val_source_updts
    except
    select * from df_spend_val_update_int""")
    df_spend_val_intmd.registerTempTable("df_spend_val_intmd")

    df_spend_final_intmd = spark.sql("""
    select 
    spnd_id,
    expense_id,
    org_expense_id,
    winning_expense_id,
    az_cust_id,
    source_system_nm,
    recipient_country_cd,
    record_company_cd,
    cust_full_nm,
    spend_date,
    submit_dt,
    spend_txnmy_id,
    updt_time,
    updt_by,
    updt_reason,
    'Updated columns not configured for comparison' as comp_col,
    '' as updt_val,
    '' as prev_val,
    latest_exp_flg
    from df_spend_val_intmd
    """)

    df_spend_final_intmd.registerTempTable("df_spend_final_intmd")

    df_spend_final_intmd1 = spark.sql("""select * from df_spend_val_update
    union all
    select * from df_spend_final_intmd
    union all
    select * from df_spend_final_no_corr
    """)
    df_spend_final_intmd1.registerTempTable("df_spend_final_intmd1")

    # append df_spend_final_intmd with  df_spend_val_update   and update source table for below df

    df_spend_final_1 = spark.sql("""
    select 
    a.winning_expense_id,
    a.spnd_id,
    a.expense_id exp_id,
    a.org_expense_id as org_exp_id,
    a.latest_exp_flg,
    a.az_cust_id,
    a.source_system_nm src_sys_nm,
    a.recipient_country_cd rpnt_ctry,
    a.record_company_cd rec_cmpy_cd,
    cast(a.spend_date as date) as spnd_dt,
    cast(a.submit_dt as date) as crean_dt,
    a.cust_full_nm cust_nm,
    a.spend_txnmy_id,
    a.comp_col,
    a.updt_val,
    a.prev_val,
    a.updt_by,
    a.updt_reason,
    a.updt_time
    from df_spend_final_intmd1 a
    group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19
    """)
    df_spend_final_1.registerTempTable("df_spend_final_1")

    df_cntry_mapping = spark.sql("""
    select ctry_cd,ctry_grp from {schema_name_cnrty_map}.{table_name_cntry_map}
    group by 1,2""".format(schema_name_cnrty_map="gbl_aspen_app_commons_test",
                           table_name_cntry_map="d_cntry_grp_map_recon"))

    df_cntry_mapping.registerTempTable("df_cntry_mapping")

    df_spend_final_ctry = spark.sql("""
        select /*+ BROADCAST(cntry_map) */ 
        a.winning_expense_id,
        a.spnd_id,
        a.exp_id,
        a.org_exp_id,
        a.latest_exp_flg,
        a.az_cust_id,
        a.src_sys_nm,
        cntry_map.ctry_grp,
        a.rpnt_ctry,
        a.rec_cmpy_cd,
        a.spnd_dt,
        a.crean_dt,
        a.cust_nm,
        a.spend_txnmy_id,
        a.comp_col,
        a.updt_val,
        a.prev_val,
        a.updt_by,
        a.updt_reason,
        a.updt_time
        from df_spend_final_1 a
        left join
        df_cntry_mapping cntry_map
        on
        trim(lower(a.rpnt_ctry))=trim(lower(cntry_map.ctry_cd))
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
        """)
    df_spend_final_ctry.registerTempTable("df_spend_final_ctry")

    df_spend_final_year = df_spend_final_ctry.withColumn('spnd_yr', F.year(F.to_timestamp('spnd_dt', 'dd/MM/yyyy')))
    df_spend_final_year.registerTempTable("df_spend_final_year")

    df_recon_current = spark.sql("""
        select distinct exp_id,
        case when lower(rptb_sta) = 'reportable' then 'R' else 'NR' end as rptb_sta
        from gbl_aspen_app_commons_test.f_dq_spend_recon where cycl_id = '20211129092300' and lower(rptb_sta) <> 'missing'
        """)
    df_recon_current.registerTempTable("df_recon_current")

    cycl_id_list = []
    result = conn.list_objects(Bucket='{datalake_bucket_cust}'.format(datalake_bucket_cust=datalake_bucket_cust),
                               Prefix='{prefix_path_dq_recon}'.format(prefix_path_dq_recon=prefix_path_dq_recon),
                               Delimiter='/')
    for o in result.get('CommonPrefixes'):
        data_path = o.get('Prefix').split('/')
        cycl_id_list.append(data_path[4])
    cycl_id_set = set(cycl_id_list)
    cycl_id_list = list(cycl_id_set)
    for i in range(0, len(cycl_id_list)):
        cycl_id_list[i] = cycl_id_list[i].replace("data_dt=", "")
        cycl_id_list[i] = int(cycl_id_list[i])
    cycl_id_list.sort()
    prev_data_dt = cycl_id_list[-2]
    prefix_path_data_dt = prefix_path_dq_recon + "data_dt=" + str(prev_data_dt) + "/"
    prefix_path_final = "s3://" + datalake_bucket_cust + "/" + prefix_path_data_dt

    f_dq_recon_prev = spark.read.parquet(prefix_path_final)
    f_dq_recon_prev.registerTempTable("f_dq_recon_prev")

    df_recon_prev = spark.sql("""
        select distinct exp_id,case when rptb_sta = 'R' then 'R' else 'NR' end as rptb_sta
        from f_dq_recon_prev where upper(rptb_sta) <> 'MISSING'
        """)
    df_recon_prev.registerTempTable("df_recon_prev")
    df_update_hist = spark.sql("""
        select 
        fin.winning_expense_id,
        fin.spnd_id,
        fin.exp_id,
        fin.org_exp_id,
        fin.latest_exp_flg,
        fin.az_cust_id,
        fin.src_sys_nm,
        fin.ctry_grp,
        fin.rpnt_ctry,
        fin.rec_cmpy_cd,
        fin.spnd_dt,
        fin.spnd_yr,
        fin.crean_dt,
        fin.cust_nm,
        fin.spend_txnmy_id,
        fin.comp_col,
        fin.updt_val,
        fin.prev_val,
        fin.updt_by,
        fin.updt_reason,
        fin.updt_time,
        case when curr.rptb_sta is NULL then 'A' else curr.rptb_sta end as curr_rptb_sta,
        case when prev.rptb_sta is NULL then 'A' else prev.rptb_sta end as prev_rptb_sta
        from 
        df_spend_final_year fin
        left join
        df_recon_current curr
        on fin.exp_id =  curr.exp_id
        left join df_recon_prev prev
        on fin.exp_id =  prev.exp_id
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23
        """)
    df_update_hist.registerTempTable("df_update_hist")
    df_winning_hist = spark.sql("""
        select winning_expense_id,max(curr_rptb_sta) as curr_sta, max(prev_rptb_sta) as prev_sta
        from df_update_hist group by winning_expense_id
        """)

    df_winning_hist.registerTempTable("df_winning_hist")
    df_final_winning_intmd = spark.sql("""
        select 
        a.winning_expense_id,
        a.spnd_id,
        a.exp_id,
        a.org_exp_id,
        a.latest_exp_flg,
        a.az_cust_id,
        a.src_sys_nm,
        a.ctry_grp,
        a.rpnt_ctry,
        a.rec_cmpy_cd,
        a.spnd_dt,
        a.spnd_yr,
        a.crean_dt,
        a.cust_nm,
        a.spend_txnmy_id,
        a.comp_col,
        a.updt_val,
        a.prev_val,
        a.updt_by,
        a.updt_reason,
        a.updt_time,
        b.curr_sta,
        b.prev_sta
        from df_update_hist a
        inner join 
        df_winning_hist b
        on a.winning_expense_id = b.winning_expense_id
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23
        """)
    df_final_winning_intmd.registerTempTable("df_final_winning_intmd")
    df_txnmy = spark.sql("""
            select spnd_txnmy_id,spnd_atvy,spnd_catg,
            spnd_type,spnd_exp from {schema_d_cmmn_txnmy}.{d_cmmn_txnmy} 
            group by 1,2,3,4,5
            """.format(schema_d_cmmn_txnmy=schema_d_cmmn_txnmy, d_cmmn_txnmy=d_cmmn_txnmy))
    df_txnmy.registerTempTable("df_txnmy")

    actual_data_dt = str(curr_cycle_id)
    actual_data_dt = actual_data_dt[0:8]
    df_spend_final_sta = spark.sql("""    
        select
        winning_expense_id,
        spnd_id,
        exp_id,
        org_exp_id,
        az_cust_id,
        src_sys_nm,
        ctry_grp,
        rpnt_ctry,
        rec_cmpy_cd,
        cast(spnd_yr as string) spnd_yr,
        spnd_dt,
        crean_dt,
        txn.spnd_atvy spnd_acty,
        txn.spnd_catg spnd_catg,
        cust_nm,
        comp_col,
        prev_val,
        updt_val,
        curr_sta,
        prev_sta, 
        case when prev_sta is NULL then 'Net New' when prev_sta = curr_sta then 'No Change' else 'Change' end as 
        rptb_sta_chng_flg,
        latest_exp_flg,
        updt_by,
        updt_reason,
        updt_time,
        "" as flex_1,"" as flex_2,"" as flex_3,"" as flex_4,"" as flex_5,{data_dt} as data_dt,  
        {curr_cycle_id} as cycl_id
        from df_final_winning_intmd a
        left join
        df_txnmy txn
        on a.spend_txnmy_id=txn.spnd_txnmy_id 
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31
        """.format(curr_cycle_id=curr_cycle_id, data_dt=actual_data_dt))
    df_spend_final_sta.registerTempTable("df_spend_final_sta")

    df_spend_final1 = spark.sql(""" 
        select * from df_spend_final_sta where spnd_yr='2021' 
        """)
    df_spend_final1.registerTempTable("df_spend_final1")

    df_filtered_a = spark.sql("""select * from df_spend_final1 where curr_sta <> 'A'""")

    df_filtered_a.registerTempTable("df_filtered_a")

    df_final_push = spark.sql("""
        select
        winning_expense_id as winning_exp_id,
        spnd_id,
        exp_id,
        org_exp_id,
        az_cust_id,
        src_sys_nm,
        ctry_grp,
        rpnt_ctry,
        rec_cmpy_cd,
        spnd_yr,
        spnd_dt,
        crean_dt,
        spnd_acty,
        spnd_catg,
        cust_nm,
        comp_col,
        prev_val,
        updt_val,
        prev_sta as prev_mnth_rptb_sta, 
        curr_sta as cur_mnth_rptb_sta,
        case when prev_sta = 'A' then 'Net New' else rptb_sta_chng_flg end as rptb_sta_chng_flg,
        latest_exp_flg,
        updt_by,
        updt_reason,
        updt_time,
        "" as flex_1,
        "" as flex_2,
        "" as flex_3,
        "" as flex_4,
        "" as flex_5,
        {actual_data_dt} as data_dt,  
        {curr_cycle_id} as cycl_id
        from df_filtered_a""".format(curr_cycle_id=curr_cycle_id, actual_data_dt=actual_data_dt))

    df_final_push.registerTempTable("df_final_push")

    df_final_push.repartition(5).write.mode("append").partitionBy("data_dt", "cycl_id").parquet(
        "{s3_path_publish}/{folder_level_1}/{folder_level_2}/{folder_level_3}/{to_table_name}/"
            .format(to_table_name=to_table_name, s3_path_publish=s3_path_publish, folder_level_1=
        folder_level_1,folder_level_2=folder_level_2, folder_level_3=folder_level_3))

    spark.sql("""ALTER TABLE {to_schema_name}.{to_table_name} recover partitions""".format(
        to_schema_name=to_schema_name, to_table_name=to_table_name))

    partitions = spark.sql(
        "SHOW PARTITIONS {schema_name_publish}.{to_table_name} ".format(schema_name_publish=schema_name_publish,
                                                                        to_table_name=to_table_name))
    listpartitions = list(partitions.select('partition').toPandas()['partition'])
    print(listpartitions)
    listpartition = []
    for a in listpartitions:
        a = a.replace('/cycl_id', '')
        listpartition.append(a)
    print(listpartition)
    cleanpartitions = [i.split('=')[1] for i in listpartition]
    actual_data_dt = actual_data_dt
    filtered = [i for i in cleanpartitions if i <= str(actual_data_dt)]
    print(filtered)

    for i in filtered:
        spark.sql(
            """alter table {schema_name_publish}.{to_table_name} DROP IF EXISTS PARTITION (data_dt = {i})""".format(
                schema_name_publish=schema_name_publish, to_table_name=to_table_name, i=i))

    df_final_push.repartition(5).write.mode("overwrite").partitionBy("data_dt", "cycl_id").parquet(
        "{s3_path_publish}/{folder_level_1_publish}/{folder_level_2_publish}/{folder_level_3_publish}/{to_table_name}/"
            .format(to_table_name=to_table_name, s3_path_publish=s3_path_publish, folder_level_1_publish=
        folder_level_1_publish, folder_level_2_publish=folder_level_2_publish, folder_level_3_publish=
                    folder_level_3_publish))

    spark.sql("""ALTER TABLE {schema_name_publish}.{to_table_name} recover partitions""".format(
        schema_name_publish=schema_name_publish, to_table_name=to_table_name))
