def run(*args, spark, s3_path, folder_level_1, folder_level_2, folder_level_3, to_table_name, to_schema_name,
        s3_path_publish,
        folder_level_1_publish, folder_level_2_publish, folder_level_3_publish, schema_name_publish, curr_cycle_id,
        schema_fact_dq, fact_dq, **kwargs):
    # import various libraries
    from pyspark.sql.functions import expr

    # fetch the data date from cycle id
    actual_data_dt = str(curr_cycle_id)
    actual_data_dt = actual_data_dt[0:8]

    # fetch the uniq id and source information from f_dq_recon fact
    df_src_comparison = spark.sql("""
    select 
    spnd_id,
    uniq_id,
    src_sys_nm,
    az_cust_id,
    ctry_grp,
    rpnt_ctry,
    rec_cmpy_cd,
    spnd_dt
    from
    {schema_fact_dq}.{fact_dq}
    where cycl_id={curr_cycle_id} and lower(rptb_sta) ='reportable'
    group by 1,2,3,4,5,6,7,8
    """.format(schema_fact_dq=schema_fact_dq, fact_dq=fact_dq, curr_cycle_id=curr_cycle_id))

    df_src_comparison.registerTempTable("df_src_comp")

    df_src_comparison_cnt = spark.sql("""
    select 
    uniq_id,
    src_sys_nm,
    az_cust_id,
    ctry_grp,
    rpnt_ctry,
    rec_cmpy_cd,
    concat(yr,"-",mth) as spnd_dt,
    src_cnt
    from
    (select
    uniq_id,
    src_sys_nm,
    az_cust_id,
    ctry_grp,
    rpnt_ctry,
    rec_cmpy_cd,
    year(spnd_dt) as yr,
    month(spnd_dt) as mth,
    count(spnd_id) as src_cnt
    from
    df_src_comp
    group by uniq_id,
    src_sys_nm,
    az_cust_id,
    ctry_grp,
    rpnt_ctry,
    rec_cmpy_cd,
    year(spnd_dt),
    month(spnd_dt)
    )
    """)

    df_src_comparison_cnt.registerTempTable("df_src_comparison_cnt")

    df_tot_src = spark.sql("""
    select 
    uniq_id as un_id,
    'Total' as src_sys_name,
    az_cust_id as cust_id,
    ctry_grp as ctry_group,
    rpnt_ctry as rpnt_ctry_cd,
    rec_cmpy_cd as rec_company_cd,
    spnd_dt as spend_dt,
    sum(src_cnt) as src_cnt1
    from
    df_src_comparison_cnt
    group by 1,2,3,4,5,6,7
    """)

    df_tot_src.registerTempTable("df_tot_src")

    # fetch SOAR records
    df_src_comp_cnt_soar = spark.sql("""
    select *
    from df_src_comparison_cnt
    where upper(src_sys_nm) = 'SOAR'
    """)
    df_src_comp_cnt_soar.registerTempTable("df_src_comp_cnt_soar")

    # identify exclusion records for TOTAl source
    df_union_exc_ttl = spark.sql("""
    select 
    ttl.un_id as uniq_id,
    ttl.src_sys_name as src_sys_nm,
    ttl.cust_id as az_cust_id,
    ttl.ctry_group as ctry_grp,
    ttl.rpnt_ctry_cd as rpnt_ctry,
    ttl.rec_company_cd as rec_cmpy_cd,
    ttl.spend_dt as spnd_dt,
    ttl.src_cnt1 as src_cnt
    from 
    df_tot_src ttl
    inner join
    df_src_comp_cnt_soar src
    on ttl.un_id = src.uniq_id and ttl.cust_id=src.az_cust_id and ttl.ctry_group = src.ctry_grp and 
    ttl.rpnt_ctry_cd=src.rpnt_ctry and ttl.rec_company_cd =src.rec_cmpy_cd and ttl.spend_dt=src.spnd_dt 
    and ttl.src_cnt1 =src.src_cnt
    group by 1,2,3,4,5,6,7,8
    """)
    df_union_exc_ttl.registerTempTable("df_union_exc_ttl")

    # identify exclusion records for SOAR source
    df_union_exc_sr = spark.sql("""
    select 
    src.uniq_id,
    src.src_sys_nm,
    src.az_cust_id,
    src.ctry_grp,
    src.rpnt_ctry,
    src.rec_cmpy_cd,
    src.spnd_dt,
    src.src_cnt
    from
    df_tot_src ttl
    inner join
    df_src_comp_cnt_soar src
    on ttl.un_id = src.uniq_id and ttl.cust_id=src.az_cust_id and ttl.ctry_group = src.ctry_grp and 
    ttl.rpnt_ctry_cd=src.rpnt_ctry and ttl.rec_company_cd =src.rec_cmpy_cd and ttl.spend_dt=src.spnd_dt 
    and ttl.src_cnt1 =src.src_cnt
    group by 1,2,3,4,5,6,7,8
    """)
    df_union_exc_sr.registerTempTable("df_union_exc_sr")

    df_union_exc = spark.sql("""
    select * from df_union_exc_sr
    union
    select * from df_union_exc_ttl""")
    df_union_exc.registerTempTable("df_union_exc")

    # union of all source records
    df_union2 = spark.sql("""
    select * from df_src_comparison_cnt
    union 
    select * from df_tot_src
    """)
    df_union2.registerTempTable("df_union2")

    # excluding duplicate records of soar and total
    df_union = spark.sql("""
    select * from df_union2
    except
    select * from df_union_exc    
    """)
    df_union.registerTempTable("df_union")

    # SELECTING EXISTING COMBINATION OF UNIQ ID, SRC_SYS_NM WITH SRC_CNT
    df_all_uniq_val = spark.sql("""select 
    uniq_id,
    src_sys_nm,
    src_cnt,
    0 as cnt
    from df_union
    group by 1,2,3
    """)

    df_all_uniq_val.registerTempTable("df_all_uniq_val")

    # SELECTING ALL DISTINCT SRC_SYS_NM WITH 0 AS JOIN CONDITION
    df_cnt_zero = spark.sql("""select 
    src_sys_nm,
    0 as cnt
    from df_union
    group by 1
    """)

    df_cnt_zero.registerTempTable("df_cnt_zero")

    # CREATING ALL COMBINATIONS OF UNIQ ID AND SRC_SYS_NM
    df_all_uniq_zero = spark.sql("""
    select 
    a.uniq_id,
    b.src_sys_nm,
    0 as src_cnt
    from df_all_uniq_val a
    left join df_cnt_zero b
    on a.cnt=b.cnt
    group by 1,2,3
    """)

    df_all_uniq_zero.registerTempTable("df_all_uniq_zero")

    # UNION ALL COMBINATIONS WITH COUNT FROM ORIGINAL AND CREATED
    df_uniq_src_union = spark.sql("""
    select uniq_id, src_sys_nm, src_cnt from df_all_uniq_val
    union
    select uniq_id, src_sys_nm, src_cnt from df_all_uniq_zero
    """)

    df_uniq_src_union.registerTempTable("df_uniq_src_union")

    # SELECTING MAX COUNT CORRESPONDING TO EACH UNIQ ID AND SRC COMBINATION
    df_uniq_max_cnt = spark.sql("""
    select uniq_id, src_sys_nm, max(src_cnt) as src_cnt
    from df_uniq_src_union
    group by 1,2
    """)

    df_uniq_max_cnt.registerTempTable("df_uniq_max_cnt")

    # CREATING RECORDS CORRESPONDING TO ALL UNIQ ID AND SRC COMBINATIONS AND SELECTING ONLY WITH COUNT=0
    df_zero_dummy_val = spark.sql("""
    select 
    uniq_id,
    src_sys_nm,
    '' as az_cust_id,
    '' as ctry_grp,
    '' as rpnt_ctry,
    '' as rec_cmpy_cd,
    '' as spnd_dt,
    src_cnt
    from df_uniq_max_cnt
    where src_cnt = 0
    group by 1,2,3,4,5,6,7,8
    """)

    df_zero_dummy_val.registerTempTable("df_zero_dummy_val")

    df_union_1 = spark.sql("""select uniq_id from df_union where src_sys_nm = 'Total' and src_cnt = 1 group by 1""")
    df_union_1.registerTempTable("df_union_1")

    # UNION ALL DATA FRAMES
    df_final1 = spark.sql("""
    select 
    a.uniq_id,
    a.src_sys_nm,
    a.az_cust_id,
    a.ctry_grp,
    a.rpnt_ctry,
    a.rec_cmpy_cd,
    a.spnd_dt,
    a.src_cnt,
    case when a.uniq_id = b.uniq_id then 'N' else 'Y' end as flg
    from df_union a
    left outer join df_union_1 b
    on a.uniq_id=b.uniq_id

    union

    select 
    uniq_id,
    src_sys_nm,
    az_cust_id,
    ctry_grp,
    rpnt_ctry,
    rec_cmpy_cd,
    spnd_dt,
    src_cnt,
    'N' as flg
    from df_zero_dummy_val
    """)

    df_final1.registerTempTable("df_final1")

    df_fnl = spark.sql(""" select  
    cast(uniq_id as string) as uniq_id,
    cast(src_sys_nm as string) as src_nm,
    cast(az_cust_id as string) as az_cust_id,
    cast(ctry_grp as string) as ctry_grp,
    cast(rpnt_ctry as string) as rpnt_ctry,
    cast(rec_cmpy_cd as string) as rec_cmpy_cd,
    cast(spnd_dt as date) spnd_dt,
    cast(src_cnt as int) as src_cnt,
    '' as flex_1,
    '' as flex_2,
    '' as flex_3,
    '' as flex_4,
    '' as flex_5,
    {data_dt} as data_dt,  
    {curr_cycle_id} as cycl_id
    from df_final1 where flg = 'N'""".format(curr_cycle_id=curr_cycle_id, data_dt=actual_data_dt))

    df_fnl.registerTempTable("df_fnl")

    df_fnl.repartition(4).write.mode("append").partitionBy("data_dt", "cycl_id").parquet(
        "{s3_path}/{folder_level_1}/{folder_level_2}/{folder_level_3}/{to_table_name}/".format(
            to_table_name=to_table_name, s3_path=s3_path, folder_level_1=folder_level_1,
            folder_level_2=folder_level_2, folder_level_3=folder_level_3))

    spark.sql("""ALTER TABLE {to_schema_name}.{to_table_name} recover partitions""".format(
        to_schema_name=to_schema_name, to_table_name=to_table_name))

    partitions = spark.sql("SHOW PARTITIONS {schema_name_publish}.{to_table_name} ".
                           format(schema_name_publish=schema_name_publish, to_table_name=to_table_name))
    listpartitions = list(partitions.select('partition').toPandas()['partition'])
    print(listpartitions)
    listpartition = []
    for a in listpartitions:
        a = a.replace('/cycl_id', '')
        listpartition.append(a)
    print(listpartition)
    cleanpartitions = [i.split('=')[1] for i in listpartition]
    actual_data_dt = actual_data_dt
    filtered = [i for i in cleanpartitions if i <= str(actual_data_dt)]
    print(filtered)

    for i in filtered:
        spark.sql("""alter table {schema_name_publish}.{to_table_name} DROP IF EXISTS PARTITION (data_dt = {i})""".
            format(
            schema_name_publish=schema_name_publish, to_table_name=to_table_name, i=i))

    df_fnl.repartition(4).write.mode("overwrite").partitionBy("data_dt", "cycl_id").parquet(
        "{s3_path_publish}/{folder_level_1_publish}/{folder_level_2_publish}/{folder_level_3_publish}/{to_table_name}/".
            format(to_table_name=to_table_name, s3_path_publish=s3_path_publish, folder_level_1_publish=
        folder_level_1_publish, folder_level_2_publish=folder_level_2_publish,
                   folder_level_3_publish=folder_level_3_publish))

    spark.sql("""ALTER TABLE {schema_name_publish}.{to_table_name} recover partitions""".format(
        schema_name_publish=schema_name_publish, to_table_name=to_table_name))

