def run(*args, spark, schema_dq_master, dq_master, curr_cycle_id, schema_dq_err_smry, dq_err_smry, schema_d_cmmn_txnmy,
        d_cmmn_txnmy, schema_f_spend, f_spend, schema_name_cnrty_map, table_name_cntry_map, s3_path,
        missing_bucket_s3_file, cncr_prefix_path,
        corr_prefix_path, soar_prefix_path, vwp_prefix_path, acty_prefix_path, bucket_name, to_table_name,
        folder_level_1, folder_level_2, folder_level_3,s3_path_publish,folder_level_1_publish,folder_level_2_publish,
        folder_level_3_publish,to_schema_name,schema_name_publish,**kwargs):
    # IMPORT VARIOUS LIBRARIES
    import pyspark.sql.functions as F
    from pyspark.sql.functions import col, concat, concat_ws
    from pyspark.sql.functions import array, explode, sort_array
    from pyspark.sql.functions import expr
    from boto3 import client
    import datetime
    from datetime import date, datetime
    import functools
    from pyspark.sql import DataFrame
    from functools import reduce
    from pyspark.sql import DataFrame
    import pandas as pd

    # FETCH THE DATA DATE FROM CYCLE ID
    actual_data_dt = str(curr_cycle_id)
    actual_data_dt = actual_data_dt[0:8]

    # FETCH THE DATA FROM DQ_MASTER_RECON
    dq_master_df = spark.sql("""
                    select 
                    dq_id,
                    dq_typ,
                    dq_grp,
                    dq_desc,
                    dq_col,
                    failr_reas,
                    crtly_ind,
                    mkt_nm,
                    src_sys_nm,
                    isactv from {schema_dq_master}.{dq_master} 
                    where trim(lower(isactv))='y' and dq_id in ('119','139')
                    group by 1,2,3,4,5,6,7,8,9,10
        """.format(schema_dq_master=schema_dq_master, dq_master=dq_master))
    dq_master_df.registerTempTable("dq_master_df")

    # FETCH THE DATA FROM DQ_ERROR SUMMARY
    dq_err_smry = spark.sql("""
            select
            dq_id,
            spnd_id,
            dq_desc,
            err_val
            from {schema_dq_err_smry}.{dq_err_smry} 
            where cycl_id={curr_cycle_id}
            and dq_id in ('119','139')
            group by 1,2,3,4
        """.format(schema_dq_err_smry=schema_dq_err_smry, dq_err_smry=dq_err_smry, curr_cycle_id=curr_cycle_id))
    dq_err_smry.registerTempTable("dq_err_smry")

    # pull taxonomy information
    df_txnmy = spark.sql("""
        select spnd_txnmy_id,spnd_atvy,spnd_catg,
        spnd_type,spnd_exp from {schema_d_cmmn_txnmy}.{d_cmmn_txnmy} 
        group by 1,2,3,4,5
        """.format(schema_d_cmmn_txnmy=schema_d_cmmn_txnmy, d_cmmn_txnmy=d_cmmn_txnmy))
    df_txnmy.registerTempTable("df_txnmy")

    # joining the dq_master and dq_err_smry df
    dq_err_master = spark.sql("""
        select /*+ BROADCAST(mastr) */ 
        mastr.dq_id,
        err.err_val,
        err.spnd_id,
        mastr.dq_desc,
        mastr.crtly_ind,
        mastr.dq_col as col_name,
        mastr.mkt_nm as mkt_nm
        from dq_err_smry err
        left join
        dq_master_df mastr
        on err.dq_id=mastr.dq_id
        group by 1,2,3,4,5,6,7
        """)
    dq_err_master.createOrReplaceTempView("dq_err_master")

    # FETCH ALL THE RECORDS FROM F_SPEND
    f_spend_r_nr = spark.sql("""
    select
        a.spnd_id,
        a.az_cust_id,
        a.az_prod_id,
        a.atendee_role,
        a.attendee_type,
        a.brand_cd,
        a.cust_full_nm,
        a.cust_full_addr,
        a.cust_class,
        a.cust_type_id,
        a.cust_sub_type_id,
        a.phone_num,
        a.dcf_code,
        a.doc_type_code,
        a.email_id,
        a.entity_paid_id,
        a.event_id,
        a.expense_id,
        a.institution,
        a.meco_id,
        a.orgnl_txnmy_id,
        a.parent_expense_id,
        a.pi_id,
        a.recipient_country,
        a.record_company_cd,
        a.row_load_dt,
        a.rptbl_spend_amount,
        a.short_notes,
        a.site_id,
        a.spend_channel_cd,
        a.spend_status_desc,
        a.spend_txnmy_id,
        a.src_cust_id,
        a.national_id,
        a.src_prod_id,
        a.study_code,
        a.fulfilment_vendor_id,
        a.vendor_number,
        a.ven_corp_grp,
        a.vendor_type,
        a.cost_center_cd,
        a.org_expense_id,
        a.recipient_country_cd,
        a.spend_date,
        a.submit_dt,
        a.src_sys_activity_num,
        a.rptblty_ind,
        a.rptbl_spend_currency_cd,
        a.usd_spend_amount,
        a.eur_spend_amount,
        a.source_system_nm
        from {schema_f_spend}.{f_spend} a
        where rptblty_ind is not null or rptblty_ind<>''
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,
        36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51
    """.format(schema_f_spend=schema_f_spend, f_spend=f_spend))
    f_spend_r_nr.registerTempTable("f_spend_r_nr")

    f_spend_it = spark.sql("""
    select 
        a.spnd_id,
        a.az_cust_id,
        a.az_prod_id,
        a.atendee_role,
        a.attendee_type,
        a.brand_cd,
        a.cust_full_nm,
        a.cust_full_addr,
        a.cust_class,
        a.cust_type_id,
        a.cust_sub_type_id,
        a.phone_num,
        a.dcf_code,
        a.doc_type_code,
        a.email_id,
        a.entity_paid_id,
        a.event_id,
        a.expense_id,
        a.institution,
        a.meco_id,
        a.orgnl_txnmy_id,
        a.parent_expense_id,
        a.pi_id,
        a.recipient_country,
        a.record_company_cd,
        a.row_load_dt,
        a.rptbl_spend_amount,
        a.short_notes,
        a.site_id,
        a.spend_channel_cd,
        a.spend_status_desc,
        a.spend_txnmy_id,
        a.src_cust_id,
        a.national_id,
        a.src_prod_id,
        a.study_code,
        a.fulfilment_vendor_id,
        a.vendor_number,
        a.ven_corp_grp,
        a.vendor_type,
        a.cost_center_cd,
        a.org_expense_id,
        a.recipient_country_cd,
        a.spend_date,
        a.submit_dt,
        a.src_sys_activity_num,
        a.rptblty_ind,
        a.rptbl_spend_currency_cd,
        a.usd_spend_amount,
        a.eur_spend_amount,
        a.source_system_nm,
        a.err_msg
        from {schema_f_spend}.{f_spend} a
        where trim(lower(a.rptblty_ind)) in ('it')
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,
        31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52
    """.format(schema_f_spend=schema_f_spend, f_spend=f_spend))
    f_spend_it.registerTempTable("f_spend_it")

    # split the error message for the IT records
    f_spend_it_err_msg = f_spend_it.select(
        "spnd_id",
        "az_cust_id",
        "az_prod_id",
        "atendee_role",
        "attendee_type",
        "brand_cd",
        "cust_full_nm",
        "cust_full_addr",
        "cust_class",
        "cust_type_id",
        "cust_sub_type_id",
        "phone_num",
        "dcf_code",
        "doc_type_code",
        "email_id",
        "entity_paid_id",
        "event_id",
        "expense_id",
        "institution",
        "meco_id",
        "orgnl_txnmy_id",
        "parent_expense_id",
        "pi_id",
        "recipient_country",
        "record_company_cd",
        "row_load_dt",
        "rptbl_spend_amount",
        "short_notes",
        "site_id",
        "spend_channel_cd",
        "spend_status_desc",
        "spend_txnmy_id",
        "src_cust_id",
        "national_id",
        "src_prod_id",
        "study_code",
        "fulfilment_vendor_id",
        "vendor_number",
        "ven_corp_grp",
        "vendor_type",
        "cost_center_cd",
        "org_expense_id",
        "recipient_country_cd",
        "spend_date",
        "submit_dt",
        "src_sys_activity_num",
        "rptblty_ind",
        "rptbl_spend_currency_cd",
        "usd_spend_amount",
        "eur_spend_amount",
        "source_system_nm",
        F.split("err_msg", ";").alias("err_msg"),
        F.posexplode(F.split("err_msg", ";")).alias("pos", "val")
    ).drop("val").select(
        "spnd_id",
        "az_cust_id",
        "az_prod_id",
        "atendee_role",
        "attendee_type",
        "brand_cd",
        "cust_full_nm",
        "cust_full_addr",
        "cust_class",
        "cust_type_id",
        "cust_sub_type_id",
        "phone_num",
        "dcf_code",
        "doc_type_code",
        "email_id",
        "entity_paid_id",
        "event_id",
        "expense_id",
        "institution",
        "meco_id",
        "orgnl_txnmy_id",
        "parent_expense_id",
        "pi_id",
        "recipient_country",
        "record_company_cd",
        "row_load_dt",
        "rptbl_spend_amount",
        "short_notes",
        "site_id",
        "spend_channel_cd",
        "spend_status_desc",
        "spend_txnmy_id",
        "src_cust_id",
        "national_id",
        "src_prod_id",
        "study_code",
        "fulfilment_vendor_id",
        "vendor_number",
        "cost_center_cd",
        "ven_corp_grp",
        "vendor_type",
        "org_expense_id",
        "recipient_country_cd",
        "spend_date",
        "submit_dt",
        "src_sys_activity_num",
        "rptblty_ind",
        "rptbl_spend_currency_cd",
        "usd_spend_amount",
        "eur_spend_amount",
        "source_system_nm",
        F.concat(F.lit("err_msg"), F.col("pos").cast("string")).alias("name"),
        F.expr("err_msg[pos]").alias("val")
    ).groupBy(
        "spnd_id",
        "az_cust_id",
        "az_prod_id",
        "atendee_role",
        "attendee_type",
        "brand_cd",
        "cust_full_nm",
        "cust_full_addr",
        "cust_class",
        "cust_type_id",
        "cust_sub_type_id",
        "phone_num",
        "dcf_code",
        "doc_type_code",
        "email_id",
        "entity_paid_id",
        "event_id",
        "expense_id",
        "institution",
        "meco_id",
        "orgnl_txnmy_id",
        "parent_expense_id",
        "pi_id",
        "recipient_country",
        "record_company_cd",
        "row_load_dt",
        "rptbl_spend_amount",
        "short_notes",
        "site_id",
        "spend_channel_cd",
        "spend_status_desc",
        "spend_txnmy_id",
        "src_cust_id",
        "national_id",
        "src_prod_id",
        "study_code",
        "fulfilment_vendor_id",
        "vendor_number",
        "cost_center_cd",
        "ven_corp_grp",
        "vendor_type",
        "org_expense_id",
        "recipient_country_cd",
        "spend_date",
        "submit_dt",
        "src_sys_activity_num",
        "rptblty_ind",
        "rptbl_spend_currency_cd",
        "usd_spend_amount",
        "eur_spend_amount",
        "source_system_nm").pivot("name").agg(F.first("val"))
    f_spend_it_err_msg.registerTempTable("f_spend_it_err_msg")

    # pivot down error msg
    col_list = f_spend_it_err_msg.columns
    cols = ["spnd_id", "az_cust_id", "az_prod_id", "atendee_role", "attendee_type", "brand_cd", "cust_full_nm",
            "cust_full_addr", "cust_class", "cust_type_id", "cust_sub_type_id", "phone_num", "dcf_code",
            "doc_type_code", "email_id", "entity_paid_id", "event_id", "expense_id", "institution", "meco_id",
            "orgnl_txnmy_id", "parent_expense_id", "pi_id", "recipient_country", "record_company_cd", "row_load_dt",
            "rptbl_spend_amount", "short_notes", "site_id", "spend_channel_cd", "spend_status_desc", "spend_txnmy_id",
            "src_cust_id", "national_id", "src_prod_id", "study_code", "fulfilment_vendor_id", "vendor_number",
            "cost_center_cd", "ven_corp_grp", "vendor_type", "org_expense_id", "recipient_country_cd", "spend_date",
            "submit_dt", "src_sys_activity_num", "rptblty_ind", "rptbl_spend_currency_cd", "usd_spend_amount",
            "eur_spend_amount", "source_system_nm"]

    for c in cols:
        col_list.remove(c)
    no_of_col = len(col_list)
    unpivotExpr = "stack(" + str(no_of_col) + ","
    for i in range(0, no_of_col):
        if i == (no_of_col - 1):
            unpivotExpr = unpivotExpr + '\'' + col_list[i] + '\',' + col_list[i] + ")"
        elif i != (no_of_col - 1):
            unpivotExpr = unpivotExpr + '\'' + col_list[i] + '\',' + col_list[i] + ','
    unpivotExpr = unpivotExpr + " as (col_name,col_value)"


    f_spend_it_err_msg_pivot_down = f_spend_it_err_msg.select("spnd_id",
                                                              "az_cust_id",
                                                              "az_prod_id",
                                                              "atendee_role",
                                                              "attendee_type",
                                                              "brand_cd",
                                                              "cust_full_nm",
                                                              "cust_full_addr",
                                                              "cust_class",
                                                              "cust_type_id",
                                                              "cust_sub_type_id",
                                                              "phone_num",
                                                              "dcf_code",
                                                              "doc_type_code",
                                                              "email_id",
                                                              "entity_paid_id",
                                                              "event_id",
                                                              "expense_id",
                                                              "institution",
                                                              "meco_id",
                                                              "orgnl_txnmy_id",
                                                              "parent_expense_id",
                                                              "pi_id",
                                                              "recipient_country",
                                                              "record_company_cd",
                                                              "row_load_dt",
                                                              "rptbl_spend_amount",
                                                              "short_notes",
                                                              "site_id",
                                                              "spend_channel_cd",
                                                              "spend_status_desc",
                                                              "spend_txnmy_id",
                                                              "src_cust_id",
                                                              "national_id",
                                                              "src_prod_id",
                                                              "study_code",
                                                              "fulfilment_vendor_id",
                                                              "vendor_number",
                                                              "cost_center_cd",
                                                              "ven_corp_grp",
                                                              "vendor_type",
                                                              "org_expense_id",
                                                              "recipient_country_cd",
                                                              "spend_date",
                                                              "submit_dt",
                                                              "src_sys_activity_num",
                                                              "rptblty_ind",
                                                              "rptbl_spend_currency_cd",
                                                              "usd_spend_amount",
                                                              "eur_spend_amount",
                                                              "source_system_nm", expr(unpivotExpr))
    f_spend_it_err_msg_pivot_down.registerTempTable("f_spend_it_err_msg_pivot_down")

    f_spend_it_err_msg_1 = spark.sql("""
        select 
        a.spnd_id,
        a.az_cust_id,
        a.az_prod_id,
        a.atendee_role,
        a.attendee_type,
        a.brand_cd,
        a.cust_full_nm,
        a.cust_full_addr,
        a.cust_class,
        a.cust_type_id,
        a.cust_sub_type_id,
        a.phone_num,
        a.dcf_code,
        a.doc_type_code,
        a.email_id,
        a.entity_paid_id,
        a.event_id,
        a.expense_id,
        a.institution,
        a.meco_id,
        a.orgnl_txnmy_id,
        a.parent_expense_id,
        a.pi_id,
        a.recipient_country,
        a.record_company_cd,
        a.row_load_dt,
        a.rptbl_spend_amount,
        a.short_notes,
        a.site_id,
        a.spend_channel_cd,
        a.spend_status_desc,
        a.spend_txnmy_id,
        a.src_cust_id,
        a.national_id,
        a.src_prod_id,
        a.study_code,
        a.fulfilment_vendor_id,
        a.vendor_number,
        a.cost_center_cd,
        a.ven_corp_grp,
        a.vendor_type,
        a.org_expense_id,
        a.recipient_country_cd,
        a.spend_date,
        a.submit_dt,
        a.src_sys_activity_num,
        a.rptblty_ind,
        a.rptbl_spend_currency_cd,
        a.usd_spend_amount,
        a.eur_spend_amount,
        a.source_system_nm,
        a.col_value as error_msg
        from f_spend_it_err_msg_pivot_down a
        where col_value <> '' and col_value is not null
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,
        35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52
    """)
    f_spend_it_err_msg_1.registerTempTable("f_spend_it_err_msg_1")

    # pull data from f_spend and join with dq_err_smry
    dq_f_spend_r_nr = spark.sql("""
        select
        a.spnd_id,
        a.az_cust_id,
        a.az_prod_id,
        a.atendee_role,
        a.attendee_type,
        a.brand_cd,
        a.cust_full_nm,
        a.cust_full_addr,
        a.cust_class,
        a.cust_type_id,
        a.cust_sub_type_id,
        a.phone_num,
        a.dcf_code,
        a.doc_type_code,
        a.email_id,
        a.entity_paid_id,
        a.event_id,
        a.expense_id,
        a.institution,
        a.meco_id,
        a.orgnl_txnmy_id,
        a.parent_expense_id,
        a.pi_id,
        a.recipient_country,
        a.record_company_cd,
        a.row_load_dt,
        a.rptbl_spend_amount,
        a.short_notes,
        a.site_id,
        a.spend_channel_cd,
        a.spend_status_desc,
        a.spend_txnmy_id,
        a.src_cust_id,
        a.national_id,
        a.src_prod_id,
        a.study_code,
        a.fulfilment_vendor_id,
        a.vendor_number,
        a.cost_center_cd,
        a.ven_corp_grp,
        a.vendor_type,
        a.org_expense_id,
        a.recipient_country_cd,
        a.spend_date,
        a.submit_dt,
        a.src_sys_activity_num,
        case when lower(a.rptblty_ind)='r' then 'Reportable'
             when lower(a.rptblty_ind)='nr' then 'Non Reportable' 
             else a.rptblty_ind end as rptblty_ind,
        a.rptbl_spend_currency_cd,
        a.usd_spend_amount,
        a.eur_spend_amount,
        a.source_system_nm,
        case when (a.recipient_country_cd = b.mkt_nm) or lower(b.mkt_nm) = 'all' then b.crtly_ind 
        when b.mkt_nm is null then '' else 'drop' end as crtly_ind,
        coalesce(b.dq_id,'999') as dq_id,
        b.err_val,
        coalesce(b.dq_desc,'No DQ Captured') as dq_desc,
        lower(b.col_name) as col_name,
        case when lower(a.rptblty_ind) = 'it' and b.dq_id is null then 'Y' else 'N' end as flg
        from f_spend_r_nr a
        left join
        dq_err_master b
        on a.spnd_id = b.spnd_id
        where a.rptblty_ind <> '' or a.rptblty_ind is not null
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,
        35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57""")
    dq_f_spend_r_nr.registerTempTable("dq_f_spend_r_nr")

    dq_f_spend_r_nr_it = spark.sql("""
    select
        a.spnd_id,
        a.az_cust_id,
        a.az_prod_id,
        a.atendee_role,
        a.attendee_type,
        a.brand_cd,
        a.cust_full_nm,
        a.cust_full_addr,
        a.cust_class,
        a.cust_type_id,
        a.cust_sub_type_id,
        a.phone_num,
        a.dcf_code,
        a.doc_type_code,
        a.email_id,
        a.entity_paid_id,
        a.event_id,
        a.expense_id,
        a.institution,
        a.meco_id,
        a.orgnl_txnmy_id,
        a.parent_expense_id,
        a.pi_id,
        a.recipient_country,
        a.record_company_cd,
        a.row_load_dt,
        a.rptbl_spend_amount,
        a.short_notes,
        a.site_id,
        a.spend_channel_cd,
        a.spend_status_desc,
        a.spend_txnmy_id,
        a.src_cust_id,
        a.national_id,
        a.src_prod_id,
        a.study_code,
        a.fulfilment_vendor_id,
        a.vendor_number,
        a.cost_center_cd,
        a.ven_corp_grp,
        a.vendor_type,
        a.org_expense_id,
        a.recipient_country_cd,
        a.spend_date,
        a.submit_dt,
        a.src_sys_activity_num,
        a.rptblty_ind,
        a.rptbl_spend_currency_cd,
        a.usd_spend_amount,
        a.eur_spend_amount,
        a.source_system_nm,
        a.crtly_ind,
        a.dq_id,
        a.err_val,
        a.dq_desc,
        col_name
        from dq_f_spend_r_nr a
        where flg='N' and crtly_ind <> 'drop'
    """)

    dq_f_spend_r_nr_it.registerTempTable("dq_f_spend_r_nr_it")

    # join the records of IT with dq master summary
    dq_f_spend_it = spark.sql("""
        select
        a.spnd_id,
        a.az_cust_id,
        a.az_prod_id,
        a.atendee_role,
        a.attendee_type,
        a.brand_cd,
        a.cust_full_nm,
        a.cust_full_addr,
        a.cust_class,
        a.cust_type_id,
        a.cust_sub_type_id,
        a.phone_num,
        a.dcf_code,
        a.doc_type_code,
        a.email_id,
        a.entity_paid_id,
        a.event_id,
        a.expense_id,
        a.institution,
        a.meco_id,
        a.orgnl_txnmy_id,
        a.parent_expense_id,
        a.pi_id,
        a.recipient_country,
        a.record_company_cd,
        a.row_load_dt,
        a.rptbl_spend_amount,
        a.short_notes,
        a.site_id,
        a.spend_channel_cd,
        a.spend_status_desc,
        a.spend_txnmy_id,
        a.src_cust_id,
        a.national_id,
        a.src_prod_id,
        a.study_code,
        a.fulfilment_vendor_id,
        a.vendor_number,
        a.cost_center_cd,
        a.ven_corp_grp,
        a.vendor_type,
        a.org_expense_id,
        a.recipient_country_cd,
        a.spend_date,
        a.submit_dt,
        a.src_sys_activity_num,
        a.rptblty_ind,
        a.rptbl_spend_currency_cd,
        a.usd_spend_amount,
        a.eur_spend_amount,
        a.source_system_nm,
        coalesce(b.crtly_ind,'Warning') crtly_ind,
        coalesce(b.dq_id,'1000') as dq_id,
        '' as err_val,
        coalesce(b.dq_desc,concat(a.error_msg,' - NOT CAPTURED IN DQ RECON MASTER CONFIG')) as dq_desc,
        lower(b.dq_col) as col_name
        from f_spend_it_err_msg_1 a
        left join 
        dq_master_df b
        on
        trim(lower(a.error_msg))=trim(lower(b.dq_desc))
        --where b.dq_id > '299'
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,
        32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56
    """)
    dq_f_spend_it.registerTempTable("dq_f_spend_it")

    # FUNCTION TO POPULATE VALUES IN ERR_VAL COLUMN ACCORDING TO COL_NAME
    def derive_column_value():
        condition = F
        for possible_value in dq_f_spend_it.columns:
            condition = condition.when(F.col("col_name") == possible_value, F.col(possible_value))
        return condition

    dq_f_spend_it = dq_f_spend_it.withColumn("err_val", derive_column_value())
    dq_f_spend_it.registerTempTable("dq_f_spend_it")

    # union of it and all records
    dq_f_spend = spark.sql("""
    select * from dq_f_spend_it
    union
    select * from dq_f_spend_r_nr_it
    """)
    dq_f_spend.registerTempTable("dq_f_spend")

    # fetch the spend id and critaclity ind and rptbl_id and create an array
    # for spend id there ac be error,warning and reportable
    dq_f_spend_repotibilty_ind = dq_f_spend.select(col("spnd_id"), col("crtly_ind").alias("rptblty_ind")).union(
        dq_f_spend.select(col("spnd_id"), col("rptblty_ind").alias("rptblty_ind"))).distinct()
    dq_f_spend_repotibilty_ind.registerTempTable("dq_f_spend_repotibilty_ind")

    dq_f_spend_repotibilty_ind_collect = dq_f_spend_repotibilty_ind.groupBy("spnd_id").agg(
        sort_array(F.collect_list("rptblty_ind")).alias("rptblty_ind_agg"))
    dq_f_spend_repotibilty_ind_collect.registerTempTable("dq_f_spend_repotibilty_ind_collect")

    dq_f_spend_repotibilty_ind_collect_str = spark.sql("""select spnd_id,
    replace(replace(replace(replace(cast(rptblty_ind_agg as string),'"',''),'[',''),']',''),', ','|') 
    as agg_rptblty_ind
    from dq_f_spend_repotibilty_ind_collect
    group by 1,2""")
    dq_f_spend_repotibilty_ind_collect_str.registerTempTable("dq_f_spend_repotibilty_ind_collect_str")

    dq_f_spend_rptblty = spark.sql("""
    select /*+ BROADCAST(b) */ 
        a.expense_id,
        a.az_cust_id,
        a.spend_txnmy_id,
        a.org_expense_id,
        a.parent_expense_id,
        a.spnd_id,
        a.recipient_country_cd,
        a.record_company_cd,
        a.spend_date,
        a.submit_dt,
        a.src_sys_activity_num,
        a.crtly_ind,
        a.rptblty_ind,
        a.rptbl_spend_amount,
        a.rptbl_spend_currency_cd,
        a.usd_spend_amount,
        a.eur_spend_amount,
        a.dq_id,
        a.err_val,
        a.dq_desc,
        a.col_name,
        a.recipient_country_cd as mkt_nm,
        a.source_system_nm,
        a.cust_full_nm,
        a.cust_class,
        b.agg_rptblty_ind
        from dq_f_spend a
        left join
        dq_f_spend_repotibilty_ind_collect_str b
        on a.spnd_id=b.spnd_id
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26
    """)
    dq_f_spend_rptblty.registerTempTable("dq_f_spend_rptblty")

    # give priority to error,warning,r,nr
    dq_f_spend_rptblty_prty = spark.sql("""
    select
        a.expense_id,
        a.az_cust_id,
        a.spend_txnmy_id,
        a.org_expense_id,
        a.parent_expense_id,
        a.spnd_id,
        a.recipient_country_cd,
        a.record_company_cd,
        a.spend_date,
        a.submit_dt,
        a.src_sys_activity_num,
        case when lower(a.agg_rptblty_ind) like '%error%' then 'Error'
             when lower(a.agg_rptblty_ind) like '%warning%' then 'Warning'
             else a.rptblty_ind end as rptblty_ind,
        a.rptbl_spend_amount,
        a.rptbl_spend_currency_cd,
        a.usd_spend_amount,
        a.eur_spend_amount,
        a.dq_id,
        a.err_val,
        a.dq_desc,
        a.col_name,
        a.mkt_nm,
        a.source_system_nm,
        a.cust_full_nm,
        a.cust_class
        from dq_f_spend_rptblty a
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24
        """)

    dq_f_spend_rptblty_prty.registerTempTable("dq_f_spend_rptblty_prty")

    # join with df_txnmy to get taxonomy information
    dq_f_spend_2 = spark.sql("""
        select 
        a.spnd_id,
        a.expense_id,
        a.az_cust_id,
        a.org_expense_id,
        a.parent_expense_id,
        a.recipient_country_cd,
        a.record_company_cd,
        a.spend_date,
        year(a.spend_date) as spend_year,
        month(a.spend_date) as spend_month,
        a.submit_dt,
        a.src_sys_activity_num,
        a.rptblty_ind,
        a.rptbl_spend_amount,
        a.rptbl_spend_currency_cd,
        a.usd_spend_amount,
        a.eur_spend_amount,
        a.dq_id,
        a.err_val,
        a.col_name,
        a.mkt_nm,
        a.dq_desc,
        a.cust_full_nm,
        a.cust_class,
        c.spnd_atvy,
        c.spnd_catg,
        c.spnd_type,
        c.spnd_exp,
        a.source_system_nm 
        from 
        dq_f_spend_rptblty_prty a
        left join 
        df_txnmy c
        on a.spend_txnmy_id=c.spnd_txnmy_id 
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29
        """)
    dq_f_spend_2.createOrReplaceTempView("dq_f_spend_2")

    # create the uniq_id
    col_list = ['az_cust_id', 'spnd_atvy', 'rptbl_spend_amount', 'spend_year', 'spend_month']

    dq_f_spend_uniq_id = dq_f_spend_2.withColumn("uniq_id", concat_ws("|", *[F.col(x) for x in col_list]))
    dq_f_spend_uniq_id.registerTempTable("dq_f_spend_uniq_id")

    # fetch the country mapping
    df_cntry_mapping = spark.sql("""
    select ctry_cd,ctry_grp from {schema_name_cnrty_map}.{table_name_cntry_map} 
    group by 1,2""".format(schema_name_cnrty_map=schema_name_cnrty_map, table_name_cntry_map=table_name_cntry_map))
    df_cntry_mapping.registerTempTable("df_cntry_mapping")

    # join with country mapping to fetch country grouping
    dq_f_spend_final = spark.sql("""select /*+ BROADCAST(cntry_map) */ 
            spnd_id,
            uniq_id,
            dq_id,
            expense_id as exp_id,
            parent_expense_id as par_exp_id,
            org_expense_id org_exp_id,
            az_cust_id,
            cust_class cust_clas,
            cust_full_nm cust_nm,
            mkt_nm,
            source_system_nm src_sys_nm,
            cntry_map.ctry_grp,
            recipient_country_cd rpnt_ctry,
            record_company_cd rec_cmpy_cd,
            cast(spend_date as date) spnd_dt,
            cast(submit_dt as date) crean_dt,
            rptblty_ind rptb_sta,
            src_sys_activity_num src_sys_acty_num,
            spnd_atvy spnd_acty,
            spnd_catg spnd_catg,
            spnd_type spnd_typ,
            spnd_exp,
            cast(rptbl_spend_amount as double) rptbl_spnd_amt,
            rptbl_spend_currency_cd rptbl_spnd_cncy_cd,
            cast(usd_spend_amount as double) usd_spnd_amt,
            cast(eur_spend_amount as double) euro_spend_amount,
            dq_desc,
            col_name,
            err_val,
            "" as flex_1,"" as flex_2,"" as flex_3,"" as flex_4,"" as flex_5,{data_dt} as data_dt,  
            {curr_cycle_id} as cycl_id
            from
            dq_f_spend_uniq_id spend
            left join
            df_cntry_mapping cntry_map
            on
            trim(lower(spend.recipient_country_cd))=trim(lower(cntry_map.ctry_cd))
    """.format(curr_cycle_id=curr_cycle_id, data_dt=actual_data_dt))
    dq_f_spend_final.registerTempTable("dq_f_spend_final")
    conn = client('s3')

    def batch_id_diff(bucket, prefix, batch_id_src, src_sys_nm, last_updt_date):
        batch_id_list = []
        result = conn.list_objects(Bucket=bucket, Prefix=prefix, Delimiter='/')
        for o in result.get('CommonPrefixes'):
            data_path = o.get('Prefix').split('/')
            batch_id_list.append(data_path[3])
        batch_id_set = set(batch_id_list)
        batch_id_list = list(batch_id_set)
        for i in range(0, len(batch_id_list)):
            batch_id_list[i] = batch_id_list[i].replace("batch_id=", "")
            batch_id_list[i] = int(batch_id_list[i])
        max_batch_id = max(batch_id_list)
        print(max_batch_id)
        batch_list = []
        prefix_path = "s3://" + bucket + "/" + prefix
        df_cncr_final = []
        for i in range(0, len(batch_id_list)):
            if batch_id_list[i] >= batch_id_src and batch_id_list[i] <= max_batch_id:
                prefix_path_batch_id = prefix_path + "batch_id=" + str(batch_id_list[i]) + "/"
                df_cncr_all_1 = spark.read.load(prefix_path_batch_id)
                df_cncr_final.append(df_cncr_all_1)
        df_cncr = functools.reduce(DataFrame.union, df_cncr_final)
        data = pd.read_csv(missing_bucket_s3_file, sep='|')
        data.loc[data.src_sys_nm == src_sys_nm, 'batch_id'] = max_batch_id
        data.loc[data.src_sys_nm == src_sys_nm, 'last_updt_dt'] = datetime.now()
        print('updated:\n', data)
        data.to_csv(missing_bucket_s3_file, sep='|', index=None, header=True)

        return df_cncr

    missing_bucket_log = spark.read.option("sep", "|").option("header", "true").csv(missing_bucket_s3_file)
    missing_bucket_log.registerTempTable("missing_bucket_log")

    batch_id_src_file_cncr = spark.sql(
        """select distinct batch_id, src_sys_nm,last_updt_dt from missing_bucket_log where src_sys_nm = 'CNCR'""")
    batch_id_src_cncr = int(batch_id_src_file_cncr.first()['batch_id'])
    src_sys_nm_cncr = batch_id_src_file_cncr.first()['src_sys_nm']
    last_updt_date = batch_id_src_file_cncr.first()['last_updt_dt']

    df_concur = batch_id_diff(bucket='{bucket_name}'.format(bucket_name=bucket_name),
                              prefix='{cncr_prefix_path}'.format(cncr_prefix_path=cncr_prefix_path),
                              batch_id_src=batch_id_src_cncr, src_sys_nm=src_sys_nm_cncr, last_updt_date=last_updt_date)

    df_concur.registerTempTable("concur_missing_bucket")

    batch_id_src_file_soar = spark.sql(
        """select distinct batch_id, src_sys_nm,last_updt_dt from missing_bucket_log where src_sys_nm = 'SOAR'""")
    batch_id_src_soar = int(batch_id_src_file_soar.first()['batch_id'])
    src_sys_nm_soar = batch_id_src_file_soar.first()['src_sys_nm']
    last_updt_date = batch_id_src_file_soar.first()['last_updt_dt']

    df_soar = batch_id_diff(bucket='{bucket_name}'.format(bucket_name=bucket_name),
                            prefix='{soar_prefix_path}'.format(soar_prefix_path=soar_prefix_path),
                            batch_id_src=batch_id_src_soar, src_sys_nm=src_sys_nm_soar, last_updt_date=last_updt_date)

    df_soar.registerTempTable("soar_missing_bucket")

    batch_id_src_file_vwp = spark.sql(
        """select distinct batch_id, src_sys_nm,last_updt_dt from missing_bucket_log where src_sys_nm = 'VWP'""")
    batch_id_src_vwp = int(batch_id_src_file_vwp.first()['batch_id'])
    src_sys_nm_vwp = batch_id_src_file_vwp.first()['src_sys_nm']
    last_updt_date = batch_id_src_file_vwp.first()['last_updt_dt']

    df_vwp = batch_id_diff(bucket='{bucket_name}'.format(bucket_name=bucket_name),
                           prefix='{vwp_prefix_path}'.format(vwp_prefix_path=vwp_prefix_path),
                           batch_id_src=batch_id_src_vwp, src_sys_nm=src_sys_nm_vwp, last_updt_date=last_updt_date)

    df_vwp.registerTempTable("vwp_missing_bucket")

    batch_id_src_file_corr = spark.sql(
        """select distinct batch_id, src_sys_nm,last_updt_dt from missing_bucket_log where src_sys_nm = 'CORR'""")
    batch_id_src_corr = int(batch_id_src_file_corr.first()['batch_id'])
    src_sys_nm_corr = batch_id_src_file_corr.first()['src_sys_nm']
    last_updt_date = batch_id_src_file_corr.first()['last_updt_dt']

    df_corr = batch_id_diff(bucket='{bucket_name}'.format(bucket_name=bucket_name),
                            prefix='{corr_prefix_path}'.format(corr_prefix_path=corr_prefix_path),
                            batch_id_src=batch_id_src_corr, src_sys_nm=src_sys_nm_corr, last_updt_date=last_updt_date)

    df_corr.registerTempTable("corr_missing_bucket")

    batch_id_src_file_acty = spark.sql(
        """select distinct batch_id, src_sys_nm,last_updt_dt from missing_bucket_log where src_sys_nm = 'ACTY'""")
    batch_id_src_acty = int(batch_id_src_file_acty.first()['batch_id'])
    src_sys_nm_acty = batch_id_src_file_acty.first()['src_sys_nm']
    last_updt_date = batch_id_src_file_acty.first()['last_updt_dt']

    df_acty = batch_id_diff(bucket='{bucket_name}'.format(bucket_name=bucket_name),
                            prefix='{acty_prefix_path}'.format(acty_prefix_path=acty_prefix_path),
                            batch_id_src=batch_id_src_acty, src_sys_nm=src_sys_nm_acty, last_updt_date=last_updt_date)

    df_acty.registerTempTable("acty_missing_bucket")

    df_concur_source = spark.sql("""select 
        '' as spnd_id,
        '' as uniq_id,
        '1001' as dq_id,
        acty_id as exp_id,
        evnt_id as par_exp_id,
        '' as  org_exp_id,
        cust_id as az_cust_id,
        ''  as cust_clas,
        concat(src_ownr_frst_nm,' ', src_ownr_last_nm) as cust_nm,
        ctry_nm as mkt_nm,
        src_sys_cd as  src_sys_nm,
        '' as ctry_grp,
        ctry_nm  as  rpnt_ctry,
        rec_cmpy_cd as  rec_cmpy_cd,
        cast(strt_dt as date) as spnd_dt,
        cast(sbmt_dt as date) crean_dt,
        'MISSING' as  rptb_sta,
        '' as  src_sys_acty_num,
        spnd_acty as  spnd_acty,
        spnd_catg as spnd_catg,
        '' as  spnd_typ,
        '' as spnd_exp,
        cast(spnd_amt as double) rptbl_spnd_amt,
        cncy_cd as  rptbl_spnd_cncy_cd,
        '' as  usd_spnd_amt,
        '' as  euro_spend_amount,
        err_ds as dq_desc,
        '' as col_name,
        '' as err_val,
        "" as flex_1,"" as flex_2,"" as flex_3,"" as flex_4,"" as flex_5,{data_dt} as data_dt,  
        {curr_cycle_id} as cycl_id
        from concur_missing_bucket""".format(curr_cycle_id=curr_cycle_id, data_dt=actual_data_dt))

    df_concur_source.registerTempTable("df_concur_source")

    df_corr_source = spark.sql("""select 
        '' as spnd_id,
        '' as uniq_id,
        '1001' as dq_id,
        src_atvy_id as exp_id,
        src_pgm_id as par_exp_id,
        '' as  org_exp_id,
        cust_id as az_cust_id,
        cust_clas  as cust_clas,
        cust_full_nm as cust_nm,
        ctry_nm as mkt_nm,
        src_sys_cd as  src_sys_nm,
        '' as ctry_grp,
        ctry_nm  as  rpnt_ctry,
        cmpy_nm as  rec_cmpy_cd,
        cast(pmt_dt as date) as spnd_dt,
        '' as  crean_dt,
        'MISSING' as  rptb_sta,
        '' as  src_sys_acty_num,
        spnd_acty as  spnd_acty,
        spnd_catg as spnd_catg,
        spnd_type as  spnd_typ,
        spnd_exp as spnd_exp,
        cast(pmt_amt as double) rptbl_spnd_amt,
        cncy_cd as  rptbl_spnd_cncy_cd,
        '' as  usd_spnd_amt,
        '' as  euro_spend_amount,
        err_ds as dq_desc,
        '' as col_name,
        '' as err_val,
        "" as flex_1,"" as flex_2,"" as flex_3,"" as flex_4,"" as flex_5,{data_dt} as data_dt,  
        {curr_cycle_id} as cycl_id
        from corr_missing_bucket""".format(curr_cycle_id=curr_cycle_id, data_dt=actual_data_dt))

    df_corr_source.registerTempTable("df_corr_source")

    df_soar_source = spark.sql("""select 
        '' as spnd_id,
        '' as uniq_id,
        '1001' as dq_id,
        concat(doc_num,'|',lin_item) as exp_id,
        '' as par_exp_id,
        '' as  org_exp_id,
        coalesce(sort_2,vend_id) as az_cust_id,
        ''  as cust_clas,
        vend_nm as cust_nm,
        vend_ctry as mkt_nm,
        'SOAR' as  src_sys_nm,
        '' as ctry_grp,
        vend_ctry  as  rpnt_ctry,
        cast(cmpy_cd as string) as  rec_cmpy_cd,
        cast(cler_dt as date) as spnd_dt,
        '' as  crean_dt,
        'MISSING' as  rptb_sta,
        '' as  src_sys_acty_num,
        gl_acct_addl_txt as  spnd_acty,
        '' as spnd_catg,
        '' as  spnd_typ,
        '' as spnd_exp,
        cast(az_doc_pmt_amt as double) rptbl_spnd_amt,
        pmt_cncy as  rptbl_spnd_cncy_cd,
        '' as  usd_spnd_amt,
        '' as  euro_spend_amount,
        err_ds as dq_desc,
        '' as col_name,
        '' as err_val,
        "" as flex_1,"" as flex_2,"" as flex_3,"" as flex_4,"" as flex_5,{data_dt} as data_dt,  
        {curr_cycle_id} as cycl_id
        from soar_missing_bucket""".format(curr_cycle_id=curr_cycle_id, data_dt=actual_data_dt))

    df_soar_source.registerTempTable("df_soar_source")

    df_vwp_source = spark.sql("""select 
        '' as spnd_id,
        '' as uniq_id,
        '1001' as dq_id,
        exp_id as exp_id,
        par_exp_id as par_exp_id,
        '' as  org_exp_id,
        az_cust_id as az_cust_id,
        ''  as cust_clas,
        autr_full_nm as cust_nm,
        rpnt_ctry as mkt_nm,
        src_sys_nm as  src_sys_nm,
        '' as ctry_grp,
        rpnt_ctry  as  rpnt_ctry,
        rec_cmpy_cd as  rec_cmpy_cd,
        cast(spnd_dt as date) as spnd_dt,
        '' as  crean_dt,
        'MISSING' as  rptb_sta,
        src_sys_acty_num as  src_sys_acty_num,
        '' as  spnd_acty,
        '' as spnd_catg,
        '' as  spnd_typ,
        '' as spnd_exp,
        cast(spnd_amt as double) rptbl_spnd_amt,
        cncy_cd as  rptbl_spnd_cncy_cd,
        '' as  usd_spnd_amt,
        '' as  euro_spend_amount,
        err_ds as dq_desc,
        '' as col_name,
        '' as err_val,
        "" as flex_1,"" as flex_2,"" as flex_3,"" as flex_4,"" as flex_5,{data_dt} as data_dt,  
        {curr_cycle_id} as cycl_id
        from vwp_missing_bucket""".format(curr_cycle_id=curr_cycle_id, data_dt=actual_data_dt))

    df_vwp_source.registerTempTable("df_vwp_source")

    df_acty_source = spark.sql("""select 
        '' as spnd_id,
        '' as uniq_id,
        '1001' as dq_id,
        intrctn_id as exp_id,
        '' as par_exp_id,
        '' as  org_exp_id,
        '' as az_cust_id,
        ''  as cust_clas,
        '' as cust_nm,
        cntry_id as mkt_nm,
        src_sys_id as  src_sys_nm,
        '' as ctry_grp,
        cntry_id  as  rpnt_ctry,
        rec_ownr_cmpny_nm as  rec_cmpy_cd,
        cast(crtd_on_dt as date) as spnd_dt,
        cast(crtd_on_dt as date) crean_dt,
        'MISSING' as  rptb_sta,
        '' as  src_sys_acty_num,
        '' as  spnd_acty,
        '' as spnd_catg,
        '' as  spnd_typ,
        '' as spnd_exp,
        '' as rptbl_spnd_amt,
        '' as  rptbl_spnd_cncy_cd,
        '' as  usd_spnd_amt,
        '' as  euro_spend_amount,
        err_ds as dq_desc,
        '' as col_name,
        '' as err_val,
        "" as flex_1,"" as flex_2,"" as flex_3,"" as flex_4,"" as flex_5,{data_dt} as data_dt,  
        {curr_cycle_id} as cycl_id
        from acty_missing_bucket""".format(curr_cycle_id=curr_cycle_id, data_dt=actual_data_dt))

    df_acty_source.registerTempTable("df_acty_source")

    def unionAll(*dfs):
        return reduce(DataFrame.unionAll, dfs)

    df_write_all = unionAll(
        *[dq_f_spend_final, df_concur_source, df_corr_source, df_soar_source, df_vwp_source, df_acty_source])

    df_write_all.registerTempTable("df_write_all")

    df_write_all.count()

    df_spend_recon_missing = spark.sql(""" select 
        cast(spnd_id as string) as spnd_id,
        cast(uniq_id as string) as uniq_id,
        cast(dq_id as string) as dq_id,
        cast(exp_id as string) as exp_id,
        cast(par_exp_id	as string) as par_exp_id,
        cast(org_exp_id	as string) as org_exp_id,
        cast(az_cust_id	as string) as az_cust_id,
        cast(cust_clas as string) as cust_clas,
        cast(cust_nm as string) as cust_nm,
        cast(mkt_nm	as string) as mkt_nm,
        cast(src_sys_nm	as string) as src_sys_nm,
        cast(ctry_grp as string) as ctry_grp,
        cast(rpnt_ctry	as string) as rpnt_ctry,
        cast(rec_cmpy_cd as string) as rec_cmpy_cd,
        cast(spnd_dt as date) as spnd_dt,
        cast(crean_dt as date) as crean_dt,
        cast(rptb_sta as string) as rptb_sta,
        cast(exp_id as string) as src_sys_acty_num,
        cast(spnd_acty as string) as spnd_acty,
        cast(spnd_catg as string) as spnd_catg,
        cast(spnd_typ as string) as spnd_typ,
        cast(spnd_exp as string) as spnd_exp,
        cast(rptbl_spnd_amt	as double) as rptbl_spnd_amt,
        cast(rptbl_spnd_cncy_cd	as string) as rptbl_spnd_cncy_cd,
        cast(usd_spnd_amt as double) as usd_spnd_amt,
        cast(euro_spend_amount as double) as euro_spend_amount,
        cast(dq_desc as string) as dq_desc,
        cast(col_name as string) as col_name,
        cast(err_val as string) as err_val,
        cast(flex_1 as string) as flex_1,
        cast(flex_2 as string) as flex_2,
        cast(flex_3 as string) as flex_3,
        cast(flex_4 as string) as flex_4,
        cast(flex_5 as string) as flex_5,
        data_dt,
        cycl_id
        from df_write_all 
    """)
    df_spend_recon_missing.registerTempTable("df_spend_recon_missing")

    df_spend_recon_missing.repartition(25).write.mode("append").partitionBy("data_dt", "cycl_id").parquet(
        "{s3_path}/{folder_level_1}/{folder_level_2}/{folder_level_3}/{to_table_name}/".format(
            to_table_name=to_table_name, s3_path=s3_path, folder_level_1=folder_level_1,
            folder_level_2=folder_level_2, folder_level_3=folder_level_3))

    spark.sql("""ALTER TABLE {to_schema_name}.{to_table_name} recover partitions""".format(
        to_schema_name=to_schema_name, to_table_name=to_table_name))

    partitions = spark.sql(
        "SHOW PARTITIONS {schema_name_publish}.{to_table_name} ".format(schema_name_publish=schema_name_publish,
                                                                        to_table_name=to_table_name))
    listpartitions = list(partitions.select('partition').toPandas()['partition'])
    print(listpartitions)
    listpartition = []
    for a in listpartitions:
        a = a.replace('/cycl_id', '')
        listpartition.append(a)
    print(listpartition)
    cleanpartitions = [i.split('=')[1] for i in listpartition]
    actual_data_dt = actual_data_dt
    filtered = [i for i in cleanpartitions if i <= str(actual_data_dt)]
    print(filtered)

    for i in filtered:
        spark.sql(
            """alter table {schema_name_publish}.{to_table_name} DROP IF EXISTS PARTITION (data_dt = {i})""".format(
                schema_name_publish=schema_name_publish, to_table_name=to_table_name, i=i))

    df_spend_recon_missing.repartition(25).write.mode("overwrite").partitionBy("data_dt", "cycl_id").parquet(
        "{s3_path_publish}/{folder_level_1_publish}/{folder_level_2_publish}/{folder_level_3_publish}/{to_table_name}/"
            .format(to_table_name=to_table_name, s3_path_publish=s3_path_publish, folder_level_1_publish=
        folder_level_1_publish,folder_level_2_publish=folder_level_2_publish, folder_level_3_publish=
        folder_level_3_publish))

    spark.sql("""ALTER TABLE {schema_name_publish}.{to_table_name} recover partitions""".format(
        schema_name_publish=schema_name_publish, to_table_name=to_table_name))