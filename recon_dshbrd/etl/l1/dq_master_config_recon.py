def run(*args, spark, from_schema_name, to_schema_name, from_table_name, to_table_name, s3_path,curr_batch_id, folder_level_1,folder_level_2,folder_level_3, **kwargs):

	partitions = spark.sql("SHOW PARTITIONS {to_schema_name}.{to_table_name} ".format(to_schema_name=to_schema_name, to_table_name=to_table_name))
	listpartitions = list(partitions.select('partition').toPandas()['partition'])
	print(listpartitions)
	cleanpartitions = [ i.split('=')[1] for i in listpartitions]
	current_batch_id=curr_batch_id
	filtered = [i for i in cleanpartitions if i <= str(current_batch_id)]
	print(filtered)
	for i in filtered:
		spark.sql("""alter table {to_schema_name}.{to_table_name} DROP IF EXISTS PARTITION (batch_id = {i})""".format(to_schema_name=to_schema_name, to_table_name=to_table_name,i=i))

	df = spark.sql(""" 
	select
  	cast(trim(upper(dq_id)) as string) dq_id,
	trim(upper(dq_typ)) dq_typ,
	trim(upper(dq_grp)) dq_grp,
	trim(upper(dq_desc)) dq_desc,
	trim(upper(dq_col)) dq_col,
	trim(upper(failr_reas)) failr_reas,
	trim(upper(crtly_ind)) crtly_ind,
	trim(upper(mkt_nm)) mkt_nm,
	trim(upper(src_sys_nm)) src_sys_nm,
	trim(upper(isactv)) isactv,
	cast(trim(upper(flex_1)) as string) flex_1,
	cast(trim(upper(flex_2)) as string) flex_2,
	cast(trim(upper(flex_3)) as string) flex_3,
	cast(trim(upper(flex_4)) as string) flex_4,
	cast(trim(upper(flex_5)) as string) flex_5,
	{curr_batch_id} as batch_id
	from {from_schema_name}.{from_table_name} a
	where batch_id = {curr_batch_id}
	""".format(from_schema_name=from_schema_name, to_schema_name=to_schema_name, from_table_name=from_table_name,to_table_name=to_table_name, curr_batch_id=curr_batch_id))

	df.write.mode("overwrite").partitionBy("batch_id").parquet(
	"{s3_path}/{folder_level_1}/{folder_level_2}/{folder_level_3}/{to_table_name}/".format(
	to_schema_name=to_schema_name, to_table_name=to_table_name, curr_batch_id=curr_batch_id, s3_path=s3_path,
	folder_level_1=folder_level_1, folder_level_2=folder_level_2, folder_level_3=folder_level_3
	))

	spark.sql(
	"""ALTER TABLE {to_schema_name}.{to_table_name} recover partitions""".format(to_schema_name=to_schema_name,to_table_name=to_table_name))