def run(*args, spark, schema_name, s3_path, table_name, folder_level_1, folder_level_2, folder_level_3, **kwargs):
    spark.sql(
        """drop table if exists {schema_name}.{table_name}""".format(schema_name=schema_name, table_name=table_name))
    spark.sql("""
            create external table if not exists {schema_name}.{table_name} (
            src_sys_nm	string,
            ctry_grp	string,
            rpnt_ctry	string,
            spnd_yr	string,
            comp_id	string,
            comp_desc	string,
            comp_curr_dt	date,
            comp_baseln_dt	date,
            tot_rptb	string,
            tot_nrptb	string,
            add_rptb	string,
            add_nrptb	string,
            del_rptb	string,
            del_nrptb	string,
            flex_1 String,
            flex_2 String,
            flex_3 String,
            flex_4 String,
            flex_5 String) 
            partitioned by (data_dt string,cycl_id bigint)
            stored as parquet
            location '{s3_path}/{folder_level_1}/{folder_level_2}/{folder_level_3}/{table_name}'
            """.format(s3_path=s3_path, schema_name=schema_name, table_name=table_name, folder_level_1=folder_level_1,
                       folder_level_2=folder_level_2, folder_level_3=folder_level_3))
    spark.sql(
        """alter table {schema_name}.{table_name} recover partitions""".format(s3_path=s3_path, schema_name=schema_name,
                                                                               table_name=table_name))

