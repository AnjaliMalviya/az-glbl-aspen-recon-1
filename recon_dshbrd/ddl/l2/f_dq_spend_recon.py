def run(*args, spark, schema_name, s3_path, table_name, folder_level_1, folder_level_2, folder_level_3, **kwargs):
    spark.sql(
        """drop table if exists {schema_name}.{table_name}""".format(schema_name=schema_name, table_name=table_name))
    spark.sql("""
            create external table if not exists {schema_name}.{table_name} (
            spnd_id	string,
            uniq_id	string,
            dq_id	string,
            exp_id	string,
            par_exp_id	string,
            org_exp_id	string,
            az_cust_id	string,
            cust_clas	string,
            cust_nm	string,
            mkt_nm	string,
            src_sys_nm	string,
            ctry_grp	string,
            rpnt_ctry	string,
            rec_cmpy_cd	string,
            spnd_dt	date,
            crean_dt	date,
            rptb_sta	string,
            src_sys_acty_num	string,
            spnd_acty	string,
            spnd_catg	string,
            spnd_typ	string,
            spnd_exp	string,
            rptbl_spnd_amt	double,
            rptbl_spnd_cncy_cd	string,
            usd_spnd_amt	double,
            euro_spend_amount	double,
            dq_desc	string,
            col_name	string,
            err_val	string,
            flex_1 String,
            flex_2 String,
            flex_3 String,
            flex_4 String,
            flex_5 String) 
            partitioned by (data_dt string,cycl_id bigint)
            stored as parquet
            location '{s3_path}/{folder_level_1}/{folder_level_2}/{folder_level_3}/{table_name}'
            """.format(s3_path=s3_path, schema_name=schema_name, table_name=table_name, folder_level_1=folder_level_1,
                       folder_level_2=folder_level_2, folder_level_3=folder_level_3))
    spark.sql(
        """alter table {schema_name}.{table_name} recover partitions""".format(s3_path=s3_path, schema_name=schema_name,
                                                                               table_name=table_name))

