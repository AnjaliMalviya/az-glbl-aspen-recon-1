def run(*args, spark, schema_name, s3_path, table_name, folder_level_1, folder_level_2, folder_level_3, **kwargs):
    spark.sql(
        """drop table if exists {schema_name}.{table_name}""".format(schema_name=schema_name, table_name=table_name))
    spark.sql("""
            create external table if not exists {schema_name}.{table_name} (
            dq_id String,
            dq_typ String,
            dq_grp String,
            dq_desc String,
            dq_col String,
            failr_reas String,
            crtly_ind String,
            mkt_nm String,
            src_sys_nm String,
            isactv String,
            flex_1 String,
            flex_2 String,
            flex_3 String,
            flex_4 String,
            flex_5 String) 
            partitioned by (batch_id bigint)
            stored as parquet
            location '{s3_path}/{folder_level_1}/{folder_level_2}/{folder_level_3}/{table_name}'
            """.format(s3_path=s3_path, schema_name=schema_name, table_name=table_name, folder_level_1=folder_level_1,
                       folder_level_2=folder_level_2, folder_level_3=folder_level_3))
    spark.sql(
        """alter table {schema_name}.{table_name} recover partitions""".format(s3_path=s3_path, schema_name=schema_name,
                                                                               table_name=table_name))
