def run(*args, spark, from_schema_name, to_schema_name, from_table_name, to_table_name, s3_path, curr_batch_id,**kwargs):

    df = spark.sql("""
    select
      <table columns>,
      monotonically_Increasing_Id() as row_id,
      src_file_name,
      batch_id
    from {from_schema_name}.{from_table_name}
    where batch_id = {curr_batch_id}
    """.format(from_schema_name=from_schema_name,from_table_name=from_table_name,curr_batch_id=curr_batch_id))


    df.write.mode("overwrite").parquet("{s3_path}/{to_schema_name}/{to_table_name}/batch_id={curr_batch_id}".format(s3_path=s3_path, to_schema_name=to_schema_name,to_table_name=to_table_name,curr_batch_id=curr_batch_id))

    spark.sql("""ALTER TABLE {to_schema_name}.{to_table_name} recover partitions""".format(to_schema_name=to_schema_name,to_table_name=to_table_name))



