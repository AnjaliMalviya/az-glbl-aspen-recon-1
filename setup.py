from distutils.core import setup

from setuptools import find_packages

SHARP_DRIVERS_VERSION = '3.2.1.dev.222'
SHARP_DRIVERS_PACKAGE_NAME = "sharp-drivers"
SHARP_DRIVERS_LOC = 'git+ssh://git@bitbucket.org/csdc/az-us-commercial-sharp-drivers.git@{version}' \
    .format(package=SHARP_DRIVERS_PACKAGE_NAME, version=SHARP_DRIVERS_VERSION)
SHARP_DRIVERS = "{package}@{loc}".format(package=SHARP_DRIVERS_PACKAGE_NAME, loc=SHARP_DRIVERS_LOC)

SHARP_CLI_VERSION = '3.2.1.dev.70'
SHARP_CLI_PACKAGE_NAME = "sharp-cli"
SHARP_CLI_LOC = 'git+ssh://git@bitbucket.org/csdc/az-us-commercial-sharp-cli.git@{version}' \
    .format(package=SHARP_CLI_PACKAGE_NAME, version=SHARP_CLI_VERSION)
SHARP_CLI = "{package}@{loc}".format(package=SHARP_CLI_PACKAGE_NAME, loc=SHARP_CLI_LOC)

setup(
    name='recon_dshbrd',
    use_scm_version={
        'version_scheme': 'post-release'
    },
    packages=find_packages(include=['recon_dshbrd.*'], exclude=['tests.*']),
    package_data={
        '': ['*.yaml', '*.json', '*.html', '*.js', '*.css', '*.png'],
    },
    install_requires=[SHARP_DRIVERS, SHARP_CLI,'pandas==1.0.5'],
    setup_requires=['setuptools_scm'],
    zip_safe=False,
)