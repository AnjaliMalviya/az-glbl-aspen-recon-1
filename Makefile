SHELL := /bin/bash

wheelhouse:
	@rm -rf wheelhouse
	@mkdir wheelhouse
	pip wheel --wheel-dir=wheelhouse -r requirements-wheel.txt
	@rm -rf dist
	python setup.py clean
	python setup.py sdist

clean:
	@echo CLEANING WORKING DIR!
	@rm -rf .pytest_cache
	@rm -rf .eggs
	@rm -rf dist
	@rm -rf sharp_drivers.egg-info
	@rm -rf spark-warehouse
	@rm -rf htmlcov
	@rm -rf test-reports
	@rm -f .coverage
	@rm -rf wheelhouse
	@rm -rf .tox
	@rm -rf env
	@rm -rf *.egg-info
	@rm -rf docs/build/*
	@find . -name "*.pyc" -exec rm -f {} \;

check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined make variable $1$(if $2, ($2))))

setup:
	@pip3 install tox
	@pip3 install virtualenv
	@pip3 install setuptools_scm


CLI_VERSION ?= none
package: SHARP_CLI_SHA = $(shell git ls-remote git@bitbucket.org:csdc/az-us-commercial-sharp-cli.git --refs $(CLI_VERSION) | cut -f1)
package: SHARP_CLI_SHA_SHORT = $(shell git rev-parse --short $(SHARP_CLI_SHA))
package: CLI_VERSION_STR = $(shell echo $(CLI_VERSION) | sed "s/[^[:alnum:]]/_/g")
package: CLI_VERSION_FILE_NAME = .cli_ver_$(CLI_VERSION_STR)_$(SHARP_CLI_SHA_SHORT)

package: clean setup
	@rm -rf wheelhouse > /dev/null 2>&1
	@rm -rf .tox > /dev/null 2>&1
	@rm -rf dist > /dev/null 2>&1
	@mkdir wheelhouse > /dev/null 2>&1
	@(	\
		export CLI_VERSION=$(CLI_VERSION) && \
		export CLI_VERSION_FILE_NAME=$(CLI_VERSION_FILE_NAME) && \
		tox -e $(shell tox -a | paste -s -d, -) \
	)



install:
	pip install wheelhouse/*
	pip install .


.PHONY: wheelhouse install clean